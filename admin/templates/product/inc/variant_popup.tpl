<!-- Modal -->
<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">Edit Variant</h4>
		</div>
		<div class="modal-body">
			<div class="row">
				{% for i in images %}
					<div class="col-lg-2 form-group">
						<label class="image-thumbnail">
							<img class="img-rounded thumbnail-image" src="{{ constants.SITE_URL }}upload/product/s_{{ i.photo_name }}">
							<input class="variant-image" type="checkbox" name="image[]" value="1" data-id="{{ i.photo_id }}" data-variant_id="{{ variant_id }}"
							{{ variantImages[i.photo_id] ? 'checked'}}>
						</label>
					</div>
				{% endfor %}
			</div>
			<div class="row">
				<div class="col-md-2">
					<div class="form-group row">
						<div class="col-md-12">
							<label class="form-control-label">Code</label>
							<div>
								<input type="hidden" name="variant_id" value="{{ variant_id }}">
								<input type="text" class="form-control"
									   name="variant_code"
									   placeholder="Code"
									   value="{{ variant.code }}">
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group row">
						<div class="col-md-12">
							<label class="form-control-label">Sale (Price)</label>
							<div>
								<input type="text" class="form-control"
									   name="variant_sale"
									   placeholder="Sale"
									   value="{{ variant.sale }}">
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group row">
						<div class="col-md-12">
							<label class="form-control-label">Sale End Date</label>

							<div class="date">
								<input type="text" class="form-control datetimepicker-3 sad"
									   name="variant_sale_end_date"
									   placeholder="Sale End Date"
									   value="{{ variant.sale_end_date }}">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		</div>
	</div>
</div>
