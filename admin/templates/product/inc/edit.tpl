{% extends "index.tpl" %}

{% block header %}
    <link rel="stylesheet" href="{{ constants.URL }}templates/assets/css/lib/jgrowl/jquery.jgrowl.css">
    <link rel="stylesheet" href="{{ constants.URL }}templates/assets/js/lib/bootstrap-fileinput-master/css/fileinput.css">
{% endblock header %}

{% block content %}
    <div class="page-content">
        <div class="container-fluid">
            <!-- Modal HTML -->
            <div id="myModal" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <!-- Content will be loaded here from "remote.php" file -->
                    </div>
                </div>
            </div>
            <form action="" method="post" enctype='multipart/form-data'>
                <header class="section-header">
                    <div class="tbl">
                        <div class="tbl-row">
                            <div class="tbl-cell">
                                <h2>{{ menuItem.title }} - Edit Product</h2>
                                <div class="subtitle"></div>
                            </div>
                        </div>
                    </div>
                </header>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-md-12">
                                <section class="tabs-section">
                                    <div class="tabs-section-nav">
                                        <div class="tbl">
                                            <ul class="nav" role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active"
                                                       href="#tabs-general" role="tab"
                                                       data-toggle="tab">
                                                        <span class="nav-link-in">General</span>
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link"
                                                       href="#tabs-photos" role="tab"
                                                       data-toggle="tab">
                                                        <span class="nav-link-in">Photos</span>
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link"
                                                       href="#tabs-variants" role="tab"
                                                       data-toggle="tab">
                                                        <span class="nav-link-in">Variants</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="tab-content">
                                        <!-- General -->
                                        <div role="tabpanel" class="tab-pane fade in active" id="tabs-general">
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <div class="form-group row">
                                                        <div class="col-md-12">
                                                            <label class="form-control-label">Published</label>

                                                            <div class="checkbox checkbox-only" for="status_id">
                                                                <input id="status_id" type="checkbox"
                                                                       name="status_id"
                                                                       value="1" {% if item[1].status_id == 1 %} checked {% endif %}>
                                                                <label for="status_id"></label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group row">
                                                        <div class="col-md-12">
                                                            <label class="form-control-label" for="featured">Featured</label>

                                                            <div class="checkbox checkbox-only">
                                                                <input id="featured" type="checkbox"
                                                                       name="featured"
                                                                       value="1" {% if item[1].featured == 1 %} checked {% endif %}>
                                                                <label for="featured"></label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group row">
                                                        <div class="col-md-12">
                                                            <label class="form-control-label">Price</label>
                                                            <div>
                                                                <input type="text" class="form-control"
                                                                       name="price"
                                                                       placeholder="Price"
                                                                       value="{{ item[1].price }}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group row">
                                                        <div class="col-md-12">
                                                            <label class="form-control-label">Sale (Price)</label>
                                                            <div>
                                                                <input type="text" class="form-control"
                                                                       name="sale"
                                                                       placeholder="Sale"
                                                                       value="{{ item[1].sale }}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group row">
                                                        <div class="col-md-12">
                                                            <label class="form-control-label">Sale End Date</label>

                                                            <div class="date">
                                                                <input type="text" class="form-control datetimepicker-1"
                                                                       name="sale_end_date"
                                                                       placeholder="Name"
                                                                       value="{{ item[1].sale_end_date }}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="form-control-label">Brand</label>
                                                        <select  class="select2" name="brand_id">
                                                            <option value="0">None</option>
                                                            {% for b in brands %}
                                                                <option value="{{ b.brand_id }}" {{ b.brand_id == item[1].brand_id ? 'selected' }}>{{ b.title }}</option>
                                                            {% endfor %}
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="form-control-label">Code</label>
                                                        <div>
                                                            <input type="text" class="form-control"
                                                                   name="code"
                                                                   placeholder="Code"
                                                                   value="{{ item[1].code }}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="form-control-label">Quantity</label>
                                                        <div>
                                                            <input type="text" class="form-control"
                                                                   name="quantity"
                                                                   placeholder="Quantity"
                                                                   value="{{ item[1].quantity }}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="form-control-label">SKU</label>
                                                        <div>
                                                            <input type="text" class="form-control"
                                                                   name="sku"
                                                                   placeholder="SKU"
                                                                   value="{{ item[1].sku }}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="form-control-label">Tags</label>
                                                        <input id="tags" type="hidden" value="{{ tags | join(',') }}">
                                                        <input id="all-tags" type="hidden" value="{{ allTags | join(',') }}">
                                                        <input class="form-control" type="text" name="tags" placeholder="Tags">
                                                    </div>
                                                </div>
                                                <div class="col-md-12 col-xs-12">
                                                    <div class="form-group">
                                                        <label class="form-control-label">Category</label>
                                                        <select  class="select2" name="categories[]" multiple="multiple">
                                                            {% for c in categories %}
                                                                <option value="{{ c.category_id }}" {{ productCategories[c.category_id] ? 'selected' }}>{{ c.title }}</option>
                                                            {% endfor %}
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <!--.tabs-section-nav-->
                                            <div class="col-lg-3 row">
                                                <div class="tabs-section-nav">
                                                    <div class="tbl">
                                                        <ul class="nav" role="tablist">
                                                            {% for key,lang in json_decode(constants.LANGS, true) %}
                                                                {% set lang = lang.id %}
                                                                <li class="nav-item">
                                                                    <a class="nav-link {% if loop.index == 1 %} active {% endif %}"
                                                                       href="#tabs-2-tab-{{ lang }}" role="tab"
                                                                       data-toggle="tab">
                                                                        <span class="nav-link-in">{{ key }}</span>
                                                                    </a>
                                                                </li>
                                                            {% endfor %}
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--.tabs-section-nav-->
                                            <div class="tab-content">
                                                {% for lang in json_decode(constants.LANGS, true) %}
                                                    {% set lang = lang.id %}
                                                    <div role="tabpanel"
                                                         class="tab-pane fade in {% if loop.index == 1 %} active {% endif %}"
                                                         id="tabs-2-tab-{{ lang }}">
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="form-group row">
                                                                    <div class="col-md-12">
                                                                        <label class="form-control-label">Title</label>

                                                                        <div class="">
                                                                            <input type="text" class="form-control"
                                                                                   name="{{ lang }}[title]"
                                                                                   placeholder="Name"
                                                                                   value="{{ item[lang].title }}">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-12">
                                                                <div class="form-group row">
                                                                    <div class="col-md-12">
                                                                        <label class="form-control-label">Description</label>

                                                                        <div class="">
                                                                    <textarea class="form-control ckeditor"
                                                                              name="{{ lang }}[desc]">{{ item[lang].desc }}</textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                {% endfor %}
                                            </div>

                                        </div>
                                        <!-- Photos -->
                                        <div role="tabpanel" class="tab-pane fade in" id="tabs-photos">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group row">
                                                        <div class="col-md-12">
                                                            <label class="control-label md-mb-15"> Photo Sizes: Small ({{ constants.PRODUCT_WIDTH_S }}X{{ constants.PRODUCT_HEIGHT_S }}),
                                                                Medium ({{ constants.PRODUCT_WIDTH_M }}X{{ constants.PRODUCT_HEIGHT_M }}),
                                                                Large ({{ constants.PRODUCT_WIDTH_L }}X{{ constants.PRODUCT_HEIGHT_L }})</label>
                                                            <input id="my-file-selector" class="file-loading" type="file" name="images[]"  multiple>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Variants -->
                                        <div role="tabpanel" class="tab-pane fade in" id="tabs-variants">
                                            <div class="container-fluid">
                                                <div class="col-xs-9">
                                                    <div class="form-group">
                                                        <h6>Manage your product </h6>
                                                        <div class="alert alert-info" role="alert">
                                                            To add combinations, enter the wanted attributes (like "size" or "color") and their respective values ("XS", "red", "all", etc.) in the field below; or select it in the right column. Then click on "Generate": it will automatically create all the combinations for you!
                                                            If you haven’t got any attributes yet, you should first create some in <a href="{{ globals.uri ~ 'attribute/' }}">Attributes</a>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-xs-9">
                                                                <input type="text" name="variants" class="form-control" placeholder="">
                                                            </div>
                                                            <div class="col-xs-3">
                                                                <button class="btn btn-default btn-block generate" data-product_id="{{ product_id }}">GENERATE</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="table-responsive">
                                                            <table class="table table-hover" id="variants-table">
                                                                <tr>
                                                                    <th>COMBINATION</th>
                                                                    <th>PRICE</th>
                                                                    <th>SKU</th>
                                                                    <th>QUANTITY</th>
                                                                    <th>DEFAULT COMBINATION</th>
                                                                </tr>
                                                                {% for v in variants %}
                                                                    <tr data-id="{{ v.variant_id }}" data-product_id="{{ v.product_id }}" class="variant-tr">
                                                                        <td>
                                                                            <div class="image-thumbnail">
                                                                                {% if v.variant_photo_name %}
                                                                                    {% set src = '' ~ constants.SITE_URL ~ 'upload/product/s_' ~ v.variant_photo_name ~ '' %}
                                                                                {% elseif v.photo_name %}
                                                                                    {% set src = '' ~ constants.SITE_URL ~ 'upload/product/s_' ~ v.photo_name ~ '' %}
                                                                                {% else %}
                                                                                    {% set src = '' ~ constants.URL ~ 'templates/assets/img/no-image.png' %}
                                                                                {% endif %}
                                                                                <img class="img-rounded thumbnail-image" src="{{ src | raw }}" alt="">
                                                                                <span>{{ v.attributes }}</span>
                                                                            </div>
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" name="variant_price" class="form-control" value="{{ v.price }}">
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" name="variant_sku" class="form-control" value="{{ v.sku }}">
                                                                        </td>
                                                                        <td>
                                                                            <input type="text" name="variant_quantity" class="form-control" value="{{ v.quantity }}">
                                                                        </td>
                                                                        <td>
                                                                            <div class="col-xs-4">
                                                                                <a href="{{ globals.uri }}product/editvariantimages/{{ v.variant_id }}/{{ v.product_id }}" data-toggle="modal"><i class="fa fa-pencil"
                                                                                                                                                             aria-hidden="true"></i></a>
                                                                            </div>
                                                                            <div class="col-xs-4">
                                                                                <a href="{{ globals.uri }}product/removevariant/{{ v.variant_id }}/" class="remove"><i class="fa fa-trash"
                                                                                                                                                                              aria-hidden="true"></i></a>
                                                                            </div>
                                                                            <div class="col-xs-4">
                                                                                <div class="radio center-radio">
                                                                                    <input type="radio" name="variant_default" id="radio-{{ v.variant_id }}" value="1" {{ v.default ? 'checked' }}>
                                                                                    <label for="radio-{{ v.variant_id }}"></label>
                                                                                </div>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                {% endfor %}
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-3">
                                                    <section class="widget widget-accordion" id="accordion" role="tablist" aria-multiselectable="true">
                                                        {% for attr_id, a in attributes %}
                                                            <article class="panel">
                                                                <div class="panel-heading" role="tab" id="heading-{{ attr_id }}">
                                                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse-{{ attr_id }}" aria-expanded="{{ loop.index == 1 ? 'true' :
                                                                    'false' }}"
                                                                       aria-controls="collapse-{{ attr_id }}" class="">
                                                                        {{ a.title }}
                                                                        <i class="font-icon font-icon-arrow-down"></i>
                                                                    </a>
                                                                </div>
                                                                <div id="collapse-{{ attr_id }}" class="panel-collapse collapse {{ loop.index == 1 ? 'in' }}" role="tabpanel"
                                                                     aria-labelledby="heading-{{ attr_id }}" aria-expanded="{{ loop.index == 1 ? 'true' :'false' }}">
                                                                    <div class="panel-collapse-in">
                                                                        {% if a.values[0].color %}
                                                                            <div class="form-group">
                                                                                <input class="form-control input-sm" type="text" id="attr-search">
                                                                            </div>
                                                                        {% endif %}
                                                                        {% for v in a.values %}
                                                                            {% if v.type_id == 2 %}
                                                                                <div class="color-block-container">
                                                                                    <div class="checkbox">
                                                                                        <input class="attr" type="checkbox" id="check-{{ v.attr_value_id }}" data-attr_value_id="{{ v.attr_value_id }}" data-title="{{ v.title ? v.title : v.color }}" data-attr_title="{{ a.title }}">
                                                                                        <label class="attr-label" for="check-{{ v.attr_value_id }}" style="{{ loop.index > 15 ? "display:none;" }}color: {{ v.color }};text-shadow: 0 0 0.5px #000;">{{ v.title ? v.title : v.color }}</label>
                                                                                    </div>
                                                                                </div>
                                                                            {% else %}
                                                                                <div class="checkbox">
                                                                                    <input class="attr" type="checkbox" id="check-{{ v.attr_value_id }}" data-attr_value_id="{{ v.attr_value_id }}" data-title="{{ v.title}}" data-attr_title="{{ a.title }}">
                                                                                    <label for="check-{{ v.attr_value_id }}">{{ v.title }}</label>
                                                                                </div>
                                                                            {% endif %}
                                                                        {% endfor %}
                                                                        {% if a.values[0].color and a.values | length > 15 %}
                                                                            <a id="show-attr" href="javascript:void(0)">Show All Colors</a>
                                                                        {% endif %}
                                                                    </div>
                                                                </div>
                                                            </article>
                                                        {% endfor %}
                                                    </section>
                                                </div>
                                            </div>
                                        </div>
                                        {% if message %}
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="alert alert-success alert-no-border alert-close alert-dismissible fade in"
                                                         role="alert">
                                                        <button type="button" class="close" data-dismiss="alert"
                                                                aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                        </button>
                                                        {{ message }}
                                                    </div>
                                                </div>
                                            </div>
                                        {% endif %}
                                    </div>
                                    <!--.tab-content-->
                                </section>
                                <!--.tabs-section-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group pull-right">
                            <button type="submit" class="btn btn-inline">Save</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
{% endblock content %}

{% block footer %}
    <script type="text/javascript" src="{{ constants.URL }}editor/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="{{ constants.URL }}templates/assets/js/lib/bootstrap-fileinput-master/plugins/sortable.min.js"></script>
    <script type="text/javascript" src="{{ constants.URL }}templates/assets/js/lib/bootstrap-fileinput-master/fileinput.js"></script>
    <script type="text/javascript" src="{{ constants.URL }}templates/assets/js/lib/jqueryui/jquery.autocomplete.js"></script>
    <script type="text/javascript" src="{{ constants.URL }}templates/assets/views/product/js/product.js"></script>
    <script type="text/javascript">
        var photos = [];
        var config = [];
        {% for img in images %}
            photos.push('{{ constants.SITE_URL }}upload/product/s_{{ img.photo_name }}');
            config.push({caption: "{{ img.photo_name }}", size: "{{ img.size }}", url: "{{ globals.uri }}product/RemoveImage/{{ img.photo_id }}", key: {{ img.photo_id }} });
        {% endfor %}

        $("#my-file-selector").fileinput({
            uploadUrl: "{{ globals.uri }}product/UploadImage/{{ item[1].product_id }}",
            uploadAsync: false,
            minFileCount: 1,
            maxFileCount: 50,
            overwriteInitial: false,
            initialPreview: photos,
            initialPreviewAsData: true, // identify if you are sending preview data only and not the raw markup
            initialPreviewFileType: 'image', // image is the default and can be overridden in config below
            initialPreviewConfig: config,
            showCaption: true,
            showClose: false,
            showUpload: true,
            showCancel: false,
            allowedFileExtensions: ['jpg','png'],
            previewSettings: {
                image: { width: "ato", height: "160px"}
            },
        }).on('filesorted', function(e, params) {
            $.ajax({
                type: 'POST',
                data: params,
                url: ADMIN_URL + LANG + '/product/SortImages/',
                success: function(data){
                    console.log(data);
                }
            });
        }).on('fileuploaded', function(e, params) {
        });
    </script>
{% endblock footer %}

