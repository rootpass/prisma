<tr>
	<th>COMBINATION</th>
	<th>PRICE</th>
	<th>SKU</th>
	<th>QUANTITY</th>
	<th>DEFAULT COMBINATION</th>
</tr>
{% for v in variants %}
	<tr data-id="{{ v.variant_id }}" data-product_id="{{ v.product_id }}" class="variant-tr">
		<td>
			<div class="image-thumbnail">
				{% if v.variant_photo_name %}
					{% set src = '' ~ constants.SITE_URL ~ 'upload/product/s_' ~ v.variant_photo_name ~ '' %}
				{% elseif v.photo_name %}
					{% set src = '' ~ constants.SITE_URL ~ 'upload/product/s_' ~ v.photo_name ~ '' %}
				{% else %}
					{% set src = '' ~ constants.URL ~ 'templates/assets/img/no-image.png' %}
				{% endif %}
				<img class="img-rounded thumbnail-image" src="{{ src | raw }}" alt="">
				<span>{{ v.attributes }}</span>
			</div>
		</td>
		<td>
			<input type="text" name="variant_price" class="form-control" value="{{ v.price }}">
		</td>
		<td>
			<input type="text" name="variant_sku" class="form-control" value="{{ v.sku }}">
		</td>
		<td>
			<input type="text" name="variant_quantity" class="form-control" value="{{ v.quantity }}">
		</td>
		<td>
			<div class="col-xs-4">
				<a href="{{ globals.uri }}product/editvariantimages/{{ v.variant_id }}/{{ v.product_id }}" data-toggle="modal"><i class="fa fa-pencil"
																																  aria-hidden="true"></i></a>
			</div>
			<div class="col-xs-4">
				<a href="{{ globals.uri }}product/removevariant/{{ v.variant_id }}/" class="remove"><i class="fa fa-trash"
																									   aria-hidden="true"></i></a>
			</div>
			<div class="col-xs-4">
				<div class="radio center-radio">
					<input type="radio" name="variant_default" id="radio-{{ v.variant_id }}" value="1" {{ v.default ? 'checked' }}>
					<label for="radio-{{ v.variant_id }}"></label>
				</div>
			</div>
		</td>
	</tr>
{% endfor %}