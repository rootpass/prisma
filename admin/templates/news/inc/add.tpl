{% extends "index.tpl" %}

{% block content %}
    <div class="page-content">
        <div class="container-fluid">
            <form action="" method="post" enctype='multipart/form-data'>
                <header class="section-header">
                    <div class="tbl">
                        <div class="tbl-row">
                            <div class="tbl-cell">
                                <h2>{{ menuItem.title }}</h2>

                                <div class="subtitle"></div>
                            </div>
                        </div>
                    </div>
                </header>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row">

                            <div class="col-md-12">
                                <section class="tabs-section">
                                    <div class="tabs-section-nav">
                                        <div class="tbl">
                                            <ul class="nav" role="tablist">
                                                {% for key,lang in json_decode(constants.LANGS, true) %}
                                                    {% set lang = lang.id %}
                                                    <li class="nav-item">
                                                        <a class="nav-link {% if loop.index == 1 %} active {% endif %}"
                                                           href="#tabs-2-tab-{{ lang }}" role="tab"
                                                           data-toggle="tab">
                                                            <span class="nav-link-in">{{ key }}</span>
                                                        </a>
                                                    </li>
                                                {% endfor %}
                                            </ul>
                                        </div>
                                    </div>
                                    <!--.tabs-section-nav-->
                                    <div class="tab-content">
                                        {% for lang in json_decode(constants.LANGS, true) %}
                                            {% set lang = lang.id %}
                                            <div role="tabpanel"
                                                 class="tab-pane fade in {% if loop.index == 1 %} active {% endif %}"
                                                 id="tabs-2-tab-{{ lang }}">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group row">
                                                            <div class="col-md-12">
                                                                <label class="form-control-label">Title</label>

                                                                <div class="">
                                                                    <input type="text" class="form-control"
                                                                           name="{{ lang }}[title]"
                                                                           placeholder="Name"
                                                                           value="{{ post.title }}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group row">
                                                            <div class="col-md-12">
                                                                <label class="form-control-label">Text</label>

                                                                <div class="">
                                                                    <textarea class="form-control ckeditor"
                                                                              name="{{ lang }}[text]">{{ post.text }}</textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group row">
                                                            <div class="col-md-12">
                                                                <label class="form-control-label">More Text</label>

                                                                <div class="">
                                                                    <textarea class="form-control ckeditor"
                                                                              name="{{ lang }}[moretext]">{{ post.moretext }}</textarea>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        {% endfor %}
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group row">
                                                    <div class="col-md-12">
                                                        <label class="form-control-label">Show title
                                                            <div class="checkbox checkbox-only">
                                                                <input id="show_title" type="checkbox"
                                                                       name="show_title"
                                                                       value="1" {% if post.show_title %} checked {% endif %}>
                                                                <label for="show_title"></label>
                                                            </div>
                                                        </label>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group row">
                                                    <div class="col-md-12">
                                                        <label class="form-control-label">Published
                                                            <div class="checkbox checkbox-only">
                                                                <input id="published" type="checkbox"
                                                                       name="published"
                                                                       value="1" {% if post.published %} checked {% endif %}>
                                                                <label for="published"></label>
                                                            </div>
                                                        </label>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group row">
                                                    <div class="col-md-12">
                                                        <label class="form-control-label">Featured
                                                            <div class="checkbox checkbox-only">
                                                                <input id="featured" type="checkbox"
                                                                       name="featured"
                                                                       value="1" {% if post.featured %} checked {% endif %}>
                                                                <label for="featured"></label>
                                                            </div>
                                                        </label>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group row">
                                                    <div class="col-md-12">
                                                        <label class="form-control-label">Published Date</label>

                                                        <div class="date">
                                                            <input type="text" class="form-control datetimepicker-1"
                                                                   name="published_date"
                                                                   placeholder="Name"
                                                                   value="{{ post.published_date }}">
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 col-xs-12">
                                                <div class="form-group">
                                                    <label class="form-control-label">Tags</label>
                                                    <input id="tags" type="hidden" value="">
                                                    <input id="all-tags" type="hidden" value="{{ allTags | join(',') }}">
                                                    <input class="form-control" type="text" name="tags" placeholder="Tags">
                                                </div>
                                            </div>
                                            <div class="col-md-12 md-mb-30">
                                                Photo Sizes: Small ({{ constants.NEWS_WIDTH_S }}X{{ constants.NEWS_HEIGHT_S }}),
                                                Large ({{ constants.NEWS_WIDTH_L }}X{{ constants.NEWS_HEIGHT_L }})
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <div class="col-md-12">
                                                        <div class="col-md-8">
                                                            <label class="btn btn-primary" for="my-file-selector">
                                                                <i class="fa fa-paperclip"></i>
                                                                <input id="my-file-selector" type="file" name="images[]" style="display:none;" onchange="$('#upload-file-info').html($(this).val());"
                                                                       data-validation="[NOTEMPTY]" multiple>
                                                                Upload Image
                                                            </label>
                                                            <span class='label label-info' id="upload-file-info"></span>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        {% if message %}
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="alert alert-success alert-no-border alert-close alert-dismissible fade in"
                                                         role="alert">
                                                        <button type="button" class="close" data-dismiss="alert"
                                                                aria-label="Close">
                                                            <span aria-hidden="true">×</span>
                                                        </button>
                                                        {{ message }}
                                                    </div>
                                                </div>
                                            </div>
                                        {% endif %}
                                    </div>
                                    <!--.tab-content-->
                                </section>
                                <!--.tabs-section-->

                            </div>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group pull-right">
                            <button type="submit" class="btn btn-inline">Save</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
{% endblock content %}

{% block footer %}
    <script type="text/javascript" src="{{ constants.URL }}editor/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="{{ constants.URL }}templates/assets/views/product/js/product.js"></script>
{% endblock footer %}
