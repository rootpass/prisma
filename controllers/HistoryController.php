<?php

class History extends Controller
{
	private $Menu;

	function __construct($Model, $ModelPath, $Menu)
	{
		$this->Menu = $Menu;
		parent::__construct($Model, $ModelPath);
		$this->Model->SetMetaData($this->Menu);
        $this->Model->SetCurrentMenu($this->Menu);
	}
	
	public function Index()
	{
        $this->Model->Params['Products'] = $this->Model->GetProducts();
        $this->View->render('history/index.tpl', $this->Model->Params);
	}
}