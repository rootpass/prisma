<?php

class Cart extends Controller
{
	private $Menu;

	function __construct($Model, $ModelPath, $Menu)
	{
		$this->Menu = $Menu;
		parent::__construct($Model, $ModelPath);
		$this->Model->SetMetaData($this->Menu);
        $this->Model->SetCurrentMenu($this->Menu);
	}

	public function Index()
	{
        $this->Model->Params['Products'] = $this->Model->GetProducts();
        $this->Model->Params['Total'] = $this->Model->Total;
        $this->View->render('cart/index.tpl', $this->Model->Params);
	}

    public function GetCart()
    {
        $this->Model->Params['Products'] = $this->Model->GetProducts();
        $this->Model->Params['Total'] = $this->Model->Total;
        $this->Model->Params['Count'] = count($this->Model->Params['Products']);
        echo $this->View->render('cart/cart.tpl', $this->Model->Params, true);
    }

    public function Add()
    {
        $Result = $this->Model->AddCart(Request::Post());
        echo json_encode($Result);
    }

    public function Remove()
    {
        $Result = $this->Model->RemoveCart(Request::Post());
        echo json_encode($Result);
    }

    public function Clear()
    {
        $Result = $this->Model->Clear();
        echo json_encode($Result);
    }

    public function Order()
    {
        $Result = $this->Model->Order();
        echo json_encode($Result);
    }
}