<?php

/**
* Main Controller
*/
class Main extends Controller
{
	private $Menu;

	function __construct($Model, $ModelPath, $Menu)
	{
		$this->Menu = $Menu;
		parent::__construct($Model, $ModelPath);
		$this->Model->SetMetaData($this->Menu);
        $this->Model->SetCurrentMenu($this->Menu);
	}

	public function Index()
	{
		$this->Model->Params['Slides'] = $this->Model->GetSlider();
		$this->Model->Params['News'] = $this->Model->GetLastNews(5);
		$this->Model->Params['Partners'] = $this->Model->GetPartners();
		$this->Model->Params['CategoriesSlider'] = $this->Model->CategoriesSlider();
		$this->Model->Params['PopularProducts'] = $this->Model->GetProducts(1, 8);
		$this->Model->Params['SaleProducts'] = $this->Model->GetProducts(2, 8);
		$this->Model->Params['LastProducts'] = $this->Model->GetProducts(3, 8);
		$this->Model->Params['FeaturedProducts'] = $this->Model->GetProducts(4, 8);
		//Functions::Pre($this->Model->Params['LastProducts']);
		$this->View->render('main/index.tpl', $this->Model->Params);
	}

}