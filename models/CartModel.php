<?php

class CartModel extends Model
{
    public $Total = 0;
    public function __construct()
	{		
		parent::__construct();
	}

    public function GetProducts()
    {
        $Cart = json_decode(Cookie::Get('cart'), true);
        //Functions::Pre(Functions::Pre($Cart));
        $Array = [];
        foreach ($Cart as $c){
            $W = ' AND v.default = 1';
            if($c['variant_id'])
                $W = ' AND v.variant_id = ' . $c['variant_id'];
            $Data = $this->DB->GetRow('SELECT
                                    p.product_id,
									v.variant_id, 
									IF(v.variant_id, v.sale, p.sale) AS sale,
									IF(v.variant_id, v.price, p.price) AS price,
									IF(v.variant_id, v.quantity, p.quantity) AS max_quantity,
								    (SELECT i.photo_name  
									FROM products_variant_photos pp
									LEFT JOIN product_photos i ON i.photo_id = pp.photo_id
									WHERE variant_id = v.variant_id LIMIT 1) as variant_photo_name,
									i.photo_name,
								    GROUP_CONCAT(at.title, ": ", avt.title) as attr,
									t.title								  
									FROM products p
									LEFT JOIN products_trans t ON t.product_id = p.product_id AND t.lang_id = ?i
									LEFT JOIN products_variants v ON v.product_id = p.product_id '.$W.'
									LEFT JOIN products_variant_attrs pva ON pva.variant_id = v.variant_id
									LEFT JOIN attr_values av ON av.attr_value_id = pva.attr_value_id
                                    LEFT JOIN attr_values_trans avt ON av.attr_value_id = avt.attr_value_id AND avt.lang_id = t.lang_id 
									LEFT JOIN attrs ON av.attr_id = attrs.attr_id
                                    LEFT JOIN attr_trans at ON attrs.attr_id = at.attr_id AND at.lang_id = t.lang_id
									LEFT JOIN product_photos i ON i.content_id = p.product_id AND i.ordering = 1
									WHERE p.product_id = ?i  AND p.status_id = 1 
									GROUP BY p.product_id ORDER BY p.product_id ASC',Lang::GetLangID(), $c['product_id']);
            $Data['quantity'] = $c['quantity'];
            $this->Total = $this->Total + $Data['quantity']*($Data['sale'] > 0 ? $Data['sale'] : $Data['price']);
            $Array[] = $Data;
        }
        //Functions::Pre($Array);
        return $Array;
    }

    public function GetQuantity($ProductId, $VariantId = 0)
    {
        if($VariantId){
            return $this->DB->GetOne('SELECT  v.quantity
                                  FROM products_variants v
                                  WHERE v.variant_id = ?i', $VariantId);
        }
        return $this->DB->GetOne('SELECT p.quantity
                                  FROM products p
                                  WHERE p.product_id = ?i', $ProductId);
    }

    public function Order()
    {
        $UserName = Request::Post('user_first_name');
        $UserPhone = Functions::CleanNumber(Request::Post('user_phone'));
        $UserEmail = Request::Post('user_email');
        $ShipingType = (int)Request::Post('shiping_type');

        if(!$UserName || !$UserPhone || !$UserEmail){
            $this->SetResult(false, 'Error');
            return $this->Result;
        }

        $Cart = json_decode(Cookie::Get('cart'), true);

        $Array = [];
        foreach ($Cart as $c){
            $W = '';
            if($c['variant_id'])
                $W = ' AND v.variant_id = ' . $c['variant_id'];
            $Data = $this->DB->GetRow('SELECT
                                    p.product_id,
                                    v.variant_id,
									IF(v.variant_id, v.price, p.price) AS price,
									IF(v.variant_id, v.sale, p.sale) AS sale			  
									FROM products p
									LEFT JOIN products_variants v ON v.product_id = p.product_id
									WHERE p.product_id = ?i '.$W.' AND p.status_id = 1 
									ORDER BY p.product_id ASC, v.default DESC LIMIT 1', $c['product_id']);

            $Data['quantity'] = $c['quantity'];
            $Data['amount'] = $Data['quantity']*($Data['sale'] > 0 ? $Data['sale'] : $Data['price']);
            $this->Total = $this->Total + $Data['amount'];
            $Array[] = $Data;
        }
        // Calculate Shiping Price
        $ShipingAmount = 0;
        $ShipingDay = 0;
        if($ShipingType == 1){
            if(SHIPING_TBILSI_FREE > $this->Total)
                $ShipingAmount = SHIPING_TBILSI_PRICE;
            $ShipingDay = SHIPING_TBILSI_DAY;
        }elseif($ShipingType == 2){
            if(SHIPING_REGION_FREE > $this->Total)
                $ShipingAmount = SHIPING_REGION_PRICE;
            $ShipingDay = SHIPING_REGION_DAY;
        }
        // Get First And Last Name
        $UserNameArray = explode(' ', $UserName);
        $this->DB->query('INSERT 
                          INTO orders 
                          SET user_first_name = ?s, user_last_name = ?s, user_phone = ?s, user_email = ?s, amount = ?s, shiping_amount = ?s, shiping_date = ?s',
            $UserNameArray[0], $UserNameArray[1], $UserPhone, $UserEmail, $this->Total, $ShipingAmount, date('Y-m-d', strtotime("+".$ShipingDay." days")));

        $OrderId = $this->DB->insertId();

        foreach ($Array as $Item)
        {
            $this->DB->query('INSERT 
                              INTO order_products 
                              SET order_id = ?i, product_id = ?i, variant_id = ?i, quantity = ?i, price = ?s',
                $OrderId, $Item['product_id'], (int)$Item['variant_id'], $Item['quantity'], $Item['amount']);
        }

        Cookie::Set('name', $UserName, time() + 60*60*24*365, '/');
        Cookie::Set('phone', $UserPhone, time() + 60*60*24*365, '/');
        Cookie::Set('email', $UserEmail, time() + 60*60*24*365, '/');

        $this->Clear();
        $this->SetResult(true, 'Success', [
            'OrderId' => $OrderId,
            'OrderAmount' => $this->Total+$ShipingAmount,
            'ShipingDate' => date('Y-m-d', strtotime("+".$ShipingDay." days"))]);
        return $this->Result;
    }

    public function AddCart()
    {
        $Update = Request::Post('update');
        $ProductId = Request::Post('product_id');
        $VariantId = (int)Request::Post('variant_id');
        $Quantity = (int)Request::Post('quantity') ? Request::Post('quantity') : 1;

        if(!$ProductId){
            $this->SetResult(false, 'Error');
            return $this->Result;
        }

        $MaxQuantity = $this->GetQuantity($ProductId, $VariantId);

        $Cart = json_decode(Cookie::Get('cart'), true);

        if(!is_array($Cart)){
            $Cart =  [['product_id' => $ProductId, 'variant_id' => $VariantId, 'quantity' => $Quantity]];
        }else{
            $NewCart = ['product_id' => $ProductId, 'variant_id' => $VariantId, 'quantity' => $Quantity];
            $Key = false;
            foreach ($Cart as $k => $c){
                if($c['product_id'] == $NewCart['product_id'] && $c['variant_id'] == $NewCart['variant_id']){
                    $Key = $k;
                    break;
                }
            }
            if($Key === false){
                $Cart[] = $NewCart;
            }
            else{
                if($Update){
                    $Quantity = $NewCart['quantity'];
                }
                else{
                    $Quantity = $Cart[$Key]['quantity']+$NewCart['quantity'];
                }

                if($Quantity > $MaxQuantity)
                    $Quantity = $MaxQuantity;
                $NewCart['quantity'] = $Quantity;
                $Cart[$Key] = $NewCart;
            }
        }
        Cookie::Set('cart', json_encode($Cart), time() + 60*60*24*365, '/');
        $this->SetResult(true, 'Success', ['Count' => count($Cart), 'quantity' => $Quantity]);
        return $this->Result;
    }

    public function RemoveCart()
    {
        $ProductId = Request::Post('product_id');
        $VariantId = (int)Request::Post('variant_id');

        if(!$ProductId){
            $this->SetResult(false, 'Error');
            return $this->Result;
        }

        $Cart = json_decode(Cookie::Get('cart'), true);

        if(is_array($Cart)){
            $CartProduct =  ['product_id' => $ProductId, 'variant_id' => $VariantId];
            $Key = false;
            foreach ($Cart as $k => $c){
                if($c['product_id'] == $CartProduct['product_id'] && $c['variant_id'] == $CartProduct['variant_id']){
                    $Key = $k;
                    break;
                }
            }
            if($Key !== false)
                unset($Cart[$Key]);
            Cookie::Set('cart', json_encode($Cart), time() + 60*60*24*365, '/');
            $this->SetResult(true, 'Success', ['Count' => count($Cart)]);
            return $this->Result;
        }else{
            $this->SetResult(false, 'Error', ['Count' => 0]);
            return $this->Result;
        }
    }

    public function Clear()
    {
        Cookie::Set('cart', json_encode([]), time() + 60*60*24*365, '/');
        $this->SetResult(true, 'Success', ['Count' => 0]);
        return $this->Result;
    }
}