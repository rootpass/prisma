<?php

class HistoryModel extends Model
{
	public function __construct()
	{		
		parent::__construct();
	}

    public function GetProducts()
    {
        $History = json_decode(Cookie::Get('history'), true);
        krsort($History);
        $In = implode(',', $History);
        $Data = [];

        if(count($History)){
            $Data = $this->DB->GetAll('SELECT
                                        p.product_id,
                                        IF(v.variant_id, v.sale, p.sale) AS sale,
									    IF(v.variant_id, v.sale_end_date, p.sale_end_date) AS sale_end_date,
									    IF(v.variant_id, v.price, p.price) AS price,
									    IF(v.variant_id, v.quantity, p.quantity) AS quantity,
                                        v.variant_id,
                                        IF(vi.photo_id, i2.photo_name, i.photo_name) AS image,
                                        t.title
                                        FROM products p
                                        LEFT JOIN products_trans t ON t.product_id = p.product_id
                                        LEFT JOIN products_variants v ON v.product_id = p.product_id AND v.default = 1
                                        LEFT JOIN product_photos i ON i.content_id = p.product_id AND i.ordering = 1
                                        LEFT JOIN products_variant_photos vi ON vi.variant_id = v.variant_id
                                        LEFT JOIN product_photos i2 ON i2.photo_id = vi.photo_id 
                                        WHERE p.product_id IN ('.$In.') AND t.lang_id = ?i AND p.status_id = 1 
                                        GROUP BY p.product_id ORDER BY FIELD(p.product_id, '.$In.') LIMIT 20', Lang::GetLangID());
        }
        return $Data;
    }
}