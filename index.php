<?php
ob_start("ob_gzhandler");
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting('E_ALL');

require_once 'loader.php';

$MyApp = new App();
$MyApp->Init();