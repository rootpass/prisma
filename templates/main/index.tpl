{% extends "index.tpl" %}

{% block content %}
    <div class="main-content main-content-box">
        <section id="section41">
            <!-- Title Description Block -->
            <div class="title-description-block title-description-block-style_5 wow fadeInUp text-center" data-wow-delay="0.6s" >
                <h3 data-size="30px" class="title-block">ახალი პროდუქტები</h3>
                <div class="desc-block">შეარჩიე შენთვის ყველაფერი საუკეთესო!</div>
            </div>
            <!--End Title Description Block -->
        </section>
        <section id="section42">
            <!-- Product Grid Description -->
            <div class="products products-grid-description equal-container">
                <div class="product-item width50">
                    <div class="product-media width50 equal-elem">
                        <div class="product-images-wrap">
                            <figure class="product-first-figure">
                                <a href="#">
                                    <img src="{{ constants.THEME }}assets/images/products/pro1.jpg" alt="img">
                                </a>
                            </figure>
                            <span class="onsale">ფასდაკლება</span>
                        </div>
                    </div>
                    <div class="product-info width50 equal-elem">
                        <div class="product-info-content">
                            <span class="product-category"><a href="#">Cold Picnic</a></span>
                            <h3 class="product-name"><a href="#">Pyramid Bead Plant Hanger</a></h3>
                            <div class="star-rating" title="Rated 5 out of 5">
                            <span>
                                <a class="star-1" href="#"><i class="fa fa-star"></i></a>
                                <a class="star-2" href="#"><i class="fa fa-star"></i></a>
                                <a class="star-3" href="#"><i class="fa fa-star"></i></a>
                                <a class="star-4" href="#"><i class="fa fa-star"></i></a>
                                <a class="star-5" href="#"><i class="fa fa-star"></i></a>
                            </span>
                            </div>
                            <div class="product-content">
                                <div class="product-desc">Pellentesque fermentum porttitor eros non tinci dunt. Aliquam nisi mauris, interdum in volutpat at, vulputate vel erat. Vestibulum ante ipsum...
                                </div>
                            </div>
                            <span class="price">
                                <ins>
                                    <span class="amount">$110.00</span>
                                </ins>
                                 <del>
                                    <span class="amount">$200.00</span>
                                </del>
                            </span>
                            <div class="product-button-action">
                                <a class="button add_to_cart_button" href="#"> Add To Cart</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="product-item width50">
                    <div class="product-media width50 equal-elem">
                        <div class="product-images-wrap">
                            <figure class="product-first-figure">
                                <a href="#">
                                    <img src="{{ constants.THEME }}assets/images/products/pro2.jpg" alt="img">
                                </a>
                            </figure>
                        </div>
                    </div>
                    <div class="product-info width50 equal-elem">
                        <div class="product-info-content">
                            <span class="product-category"><a href="#">A.P.C</a></span>
                            <h3 class="product-name"><a href="#">Bougie Jasmin Vert</a></h3>
                            <div class="star-rating" title="Rated 5 out of 5">
                                <span>
                                    <a class="star-1" href="#"><i class="fa fa-star"></i></a>
                                    <a class="star-2" href="#"><i class="fa fa-star"></i></a>
                                    <a class="star-3" href="#"><i class="fa fa-star"></i></a>
                                    <a class="star-4" href="#"><i class="fa fa-star"></i></a>
                                    <a class="star-5" href="#"><i class="fa fa-star"></i></a>
                                </span>
                            </div>
                            <div class="product-content">
                                <div class="product-desc">Pellentesque fermentum porttitor eros non tinci dunt. Aliquam nisi mauris, interdum in volutpat at, vulputate vel erat. Vestibulum ante ipsum...
                                </div>
                            </div>
                            <span class="price">
                                <span class="amount">$50.00</span>
                            </span>
                            <div class="product-button-action">
                                <a class="button add_to_cart_button" href="#"> Add To Cart</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="product-item width50">
                    <div class="product-media width50 pull-right equal-elem">
                        <div class="product-images-wrap">
                            <figure class="product-first-figure">
                                <a href="#">
                                    <img src="{{ constants.THEME }}assets/images/products/pro3.jpg" alt="img">
                                </a>
                            </figure>
                            <span class="new-product-light">New</span>
                        </div>
                    </div>
                    <div class="product-info width50 equal-elem">
                        <div class="product-info-content">
                            <span class="product-category"><a href="#">Anglepoise</a></span>
                            <h3 class="product-name"><a href="#">Type 75 Margaret Howell Lamp</a></h3>
                            <div class="star-rating" title="Rated 5 out of 5">
                                <span>
                                    <a class="star-1" href="#"><i class="fa fa-star"></i></a>
                                    <a class="star-2" href="#"><i class="fa fa-star"></i></a>
                                    <a class="star-3" href="#"><i class="fa fa-star"></i></a>
                                    <a class="star-4" href="#"><i class="fa fa-star"></i></a>
                                    <a class="star-5" href="#"><i class="fa fa-star"></i></a>
                                </span>
                            </div>
                            <div class="product-content">
                                <div class="product-desc">Pellentesque fermentum porttitor eros non tinci dunt. Aliquam nisi mauris, interdum in volutpat at, vulputate vel erat. Vestibulum ante ipsum...
                                </div>
                            </div>
                            <span class="price">
                                    <span class="amount">$24.00</span>
                            </span>
                            <div class="product-button-action">
                                <a class="button add_to_cart_button" href="#"> Add To Cart</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="product-item width50">
                    <div class="product-media width50 pull-right equal-elem">
                        <div class="product-images-wrap">
                            <figure class="product-first-figure">
                                <a href="#">
                                    <img src="{{ constants.THEME }}assets/images/products/pro4.jpg" alt="img">
                                </a>
                            </figure>
                        </div>
                    </div>
                    <div class="product-info width50 equal-elem">
                        <div class="product-info-content">
                            <span class="product-category"><a href="#">Eric Trine</a></span>
                            <h3 class="product-name"><a href="#">Double Octahedron Pedestal</a></h3>
                            <div class="star-rating" title="Rated 5 out of 5">
                                <span>
                                    <a class="star-1" href="#"><i class="fa fa-star"></i></a>
                                    <a class="star-2" href="#"><i class="fa fa-star"></i></a>
                                    <a class="star-3" href="#"><i class="fa fa-star"></i></a>
                                    <a class="star-4" href="#"><i class="fa fa-star"></i></a>
                                    <a class="star-5" href="#"><i class="fa fa-star"></i></a>
                                </span>
                            </div>
                            <div class="product-content">
                                <div class="product-desc">Pellentesque fermentum porttitor eros non tinci dunt. Aliquam nisi mauris, interdum in volutpat at, vulputate vel erat. Vestibulum ante ipsum...
                                </div>
                            </div>
                            <span class="price">
                                <span class="amount">$95.00</span>
                            </span>
                            <div class="product-button-action">
                                <a class="button add_to_cart_button" href="#"> Add To Cart</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="product-item width50 ">
                    <div class="product-media width50 equal-elem">
                        <div class="product-images-wrap">
                            <figure class="product-first-figure">
                                <a href="#">
                                    <img src="{{ constants.THEME }}assets/images/products/pro5.jpg" alt="img">
                                </a>
                            </figure>
                        </div>
                    </div>
                    <div class="product-info width50 equal-elem">
                        <div class="product-info-content">
                            <span class="product-category"><a href="#">Architectmade</a></span>
                            <h3 class="product-name"><a href="#">Large Bird in Natural</a></h3>
                            <div class="star-rating" title="Rated 5 out of 5">
                                <span>
                                    <a class="star-1" href="#"><i class="fa fa-star"></i></a>
                                    <a class="star-2" href="#"><i class="fa fa-star"></i></a>
                                    <a class="star-3" href="#"><i class="fa fa-star"></i></a>
                                    <a class="star-4" href="#"><i class="fa fa-star"></i></a>
                                    <a class="star-5" href="#"><i class="fa fa-star"></i></a>
                                </span>
                            </div>
                            <div class="product-content">
                                <div class="product-desc">Pellentesque fermentum porttitor eros non tinci dunt. Aliquam nisi mauris, interdum in volutpat at, vulputate vel erat. Vestibulum ante ipsum...
                                </div>
                            </div>
                            <span class="price">
                                <span class="amount">$79.00</span>
                            </span>
                            <div class="product-button-action">
                                <a class="button add_to_cart_button" href="#"> Add To Cart</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="product-item width50">
                    <div class="product-media width50 equal-elem">
                        <div class="product-images-wrap">
                            <figure class="product-first-figure">
                                <a href="#">
                                    <img src="{{ constants.THEME }}assets/images/products/pro6.jpg" alt="img">
                                </a>
                            </figure>
                        </div>
                    </div>
                    <div class="product-info width50 equal-elem">
                        <div class="product-info-content">
                            <span class="product-category"><a href="#">Hawkins New York</a></span>
                            <h3 class="product-name"><a href="#">Simple Linen Pillow</a></h3>
                            <div class="star-rating" title="Rated 5 out of 5">
                                <span>
                                    <a class="star-1" href="#"><i class="fa fa-star"></i></a>
                                    <a class="star-2" href="#"><i class="fa fa-star"></i></a>
                                    <a class="star-3" href="#"><i class="fa fa-star"></i></a>
                                    <a class="star-4" href="#"><i class="fa fa-star"></i></a>
                                    <a class="star-5" href="#"><i class="fa fa-star"></i></a>
                                </span>
                            </div>
                            <div class="product-content">
                                <div class="product-desc">Pellentesque fermentum porttitor eros non tinci dunt. Aliquam nisi mauris, interdum in volutpat at, vulputate vel erat. Vestibulum ante ipsum...
                                </div>
                            </div>
                            <span class="price">
                                <ins>
                                    <span class="amount">$110</span>
                                </ins>
                                 <del>
                                    <span class="amount">$200</span>
                                </del>
                            </span>
                            <div class="product-button-action">
                                <a class="button add_to_cart_button" href="#"> Add To Cart</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="product-item width50">
                    <div class="product-media width50 pull-right equal-elem">
                        <div class="product-images-wrap">
                            <figure class="product-first-figure">
                                <a href="#">
                                    <img src="{{ constants.THEME }}assets/images/products/pro7.jpg" alt="img">
                                </a>
                            </figure>
                        </div>
                    </div>
                    <div class="product-info width50 equal-elem">
                        <div class="product-info-content">
                            <span class="product-category"><a href="#">Bath</a></span>
                            <h3 class="product-name"><a href="#">Hammam Beach Towel</a></h3>
                            <div class="star-rating" title="Rated 5 out of 5">
                                <span>
                                    <a class="star-1" href="#"><i class="fa fa-star"></i></a>
                                    <a class="star-2" href="#"><i class="fa fa-star"></i></a>
                                    <a class="star-3" href="#"><i class="fa fa-star"></i></a>
                                    <a class="star-4" href="#"><i class="fa fa-star"></i></a>
                                    <a class="star-5" href="#"><i class="fa fa-star"></i></a>
                                </span>
                            </div>
                            <div class="product-content">
                                <div class="product-desc">Pellentesque fermentum porttitor eros non tinci dunt. Aliquam nisi mauris, interdum in volutpat at, vulputate vel erat. Vestibulum ante ipsum...
                                </div>
                            </div>
                            <span class="price">
                                <span class="amount">$48.00</span>
                            </span>
                            <div class="product-button-action">
                                <a class="button add_to_cart_button" href="#"> Add To Cart</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="product-item width50">
                    <div class="product-media width50 pull-right equal-elem">
                        <div class="product-images-wrap">
                            <figure class="product-first-figure">
                                <a href="#">
                                    <img src="{{ constants.THEME }}assets/images/products/pro8.jpg" alt="img">
                                </a>
                            </figure>
                            <span class="new-product">New</span>
                        </div>
                    </div>
                    <div class="product-info width50 equal-elem">
                        <div class="product-info-content">
                            <span class="product-category"><a href="#">Hasami Porcelain</a></span>
                            <h3 class="product-name"><a href="#">Creative Tray Triangle</a></h3>
                            <div class="star-rating" title="Rated 5 out of 5">
                                <span>
                                    <a class="star-1" href="#"><i class="fa fa-star"></i></a>
                                    <a class="star-2" href="#"><i class="fa fa-star"></i></a>
                                    <a class="star-3" href="#"><i class="fa fa-star"></i></a>
                                    <a class="star-4" href="#"><i class="fa fa-star"></i></a>
                                    <a class="star-5" href="#"><i class="fa fa-star"></i></a>
                                </span>
                            </div>
                            <div class="product-content">
                                <div class="product-desc">Pellentesque fermentum porttitor eros non tinci dunt. Aliquam nisi mauris, interdum in volutpat at, vulputate vel erat. Vestibulum ante ipsum...
                                </div>
                            </div>
                            <span class="price">
                                <span class="amount">$65.00</span>
                            </span>
                            <div class="product-button-action">
                                <a class="button add_to_cart_button" href="#"> Add To Cart</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Product Grid Description -->
        </section>
        <section id="section43">
            <!-- Title Description Block -->
            <div class="title-description-block title-description-block-style_5 wow fadeInUp text-center" data-wow-delay="0.6s" >
                <h3 data-size="30px" class="title-block">პირობები & წესები </h3>
<!--                 <div class="desc-block">Find more our Featured products!</div> -->
            </div>
            <!--End Title Description Block -->
        </section>
        <section id="section44">
            <div class="container">
                <!-- icon box -->
                <div class="row">
                    <div class="col-sm-4">
                        <div data-wow-delay="1s" class="icon-box icon-box-style5 wow zoomIn">
                            <span class="font-icon">
                                <span class="flaticon-rocket"></span>
                            </span>
                            <div class="icon-box-content">
                                <h5 class="icon-box-title"><a href="#">მიტანის სერვისი</a></h5>
                                <p class="icon-box-desc">Pellentesque fermentum porttitor eros non tinci
                                    dunt. Aliquam nisi mauris, interdum in volutpat atvulpu tate vel erat. Vestibulum ante ipsum...</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div data-wow-delay="1s" class="icon-box icon-box-style5 wow zoomIn">
                            <span class="font-icon">
                                <span class="flaticon-lock"></span>
                            </span>
                            <div class="icon-box-content">
                                <h5 class="icon-box-title"><a href="#">საიტის წესები</a></h5>
                                <p class="icon-box-desc">Pellentesque fermentum porttitor eros non tinci
                                    dunt. Aliquam nisi mauris, interdum in volutpat atvulpu tate vel erat. Vestibulum ante ipsum...</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div data-wow-delay="1s" class="icon-box icon-box-style5 wow zoomIn">
                            <span class="font-icon">
                                <span class="flaticon-passage-of-time"></span>
                            </span>
                            <div class="icon-box-content">
                                <h5 class="icon-box-title"><a href="#">შეკვეთის პირობები</a></h5>
                                <p class="icon-box-desc">Pellentesque fermentum porttitor eros non tinci
                                    dunt. Aliquam nisi mauris, interdum in volutpat atvulpu tate vel erat. Vestibulum ante ipsum...</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Newsletter -->
        </section>
    </div>
{% endblock %}