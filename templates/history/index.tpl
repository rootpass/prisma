{% extends "index.tpl" %}

{% block content %}
    <section class="breadcrumbs  hidden-xs">
        <div class="container">
            <ol class="breadcrumb breadcrumb--wd pull-left">
                <li><a href="{{ globals.uri }}">{{ langs.main }}</a></li>
                <li class="active">{{ langs.saved_products }}</li>
            </ol>
        </div>
    </section>
    <!-- Content section -->
    <section class="content">
        <div class="container">
            <h2 class="text-uppercase text-xs-center">{{ langs.history }}</h2>
            <div class="card card--padding">
                <table class="table shopping-cart-table">
                    <thead>
                    <tr class="text-uppercase">
                        <th>&nbsp;</th>
                        <th class="text-left">{{ langs.product_name }}</th>
                        <th class="text-center">{{ langs.price }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    {% if Products | length == 0 %}
                        <tr class="container text-center"><td colspan="3"><h5>{{ langs.products_not_found }}</h5></td></tr>
                    {% endif %}
                    {% for p in Products %}
                        <tr class="text-center" id="{{ p.product_id }}">
                            <td class="no-title image-col text-left">
                                {% if p.image %}
                                    {% set src = constants.UPLOAD ~ 'product/s_' ~ p.image %}
                                {% else %}
                                    {% set src = constants.THEME ~ 'assets/images/products/no_photo_s.jpg' %}
                                {% endif %}
                                <div class="shopping-cart-table__product-image"><a href="{{ globals.uri }}product/detail/{{ p.product_id }}/{{ p.variant_id }}">
                                        <img src="{{ src }}" alt=""/>
                                    </a>
                                </div>
                            </td>
                            <td class="text-left">
                                <div class="th-title visible-xs">{{ p.title }}:</div>
                                <h6 class="shopping-cart-table__product-name text-left text-uppercase"><a href="{{ globals.uri }}product/detail/{{ p.product_id }}/{{ p.variant_id }}">{{ p.title }}</a></h6></td>
                            <td>
                                <div class="th-title visible-xs">{{ langs.price }}:</div>
                                <div class="shopping-cart-table__product-price">{{ p.price > 0 ? p.price ~ langs.valute_symbol : p.sale ~ langs.valute_symbol }}</div>
                            </td>
                        </tr>
                    {% endfor %}
                    </tbody>
                </table>
            </div>
        </div>
        <div class="divider divider--lg"></div>
    </section>
    <!-- End Content section -->
{% endblock %}