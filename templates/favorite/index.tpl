{% extends "index.tpl" %}

{% block content %}
    <section class="breadcrumbs  hidden-xs">
        <div class="container">
            <ol class="breadcrumb breadcrumb--wd pull-left">
                <li><a href="{{ globals.uri }}">{{ langs.main }}</a></li>
                <li class="active">{{ langs.saved_products }}</li>
            </ol>
        </div>
    </section>
    <!-- Content section -->
    <section class="content">
        <div class="container">
            <h2 class="text-uppercase text-xs-center">{{ langs.saved_products }}</h2>
            <div class="card card--padding">
                <table class="table shopping-cart-table">
                    <thead>
                    <tr class="text-uppercase">
                        <th>&nbsp;</th>
                        <th class="text-left">{{ langs.product_name }}</th>
                        <th class="text-center">{{ langs.price }}</th>
                        <th class="text-center">{{ langs.delete }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="cart-empty {{ Favorites | length ? 'hidden' }} text-center">
                        <td colspan="4"><h5>{{ langs.products_not_found }}</h5></td>
                    </tr>
                    {% for f in Favorites %}
                        <tr class="favorite-tr text-center" id="{{ f.product_id }}">
                            <td class="no-title image-col text-left">
                                {% if f.image %}
                                    {% set src = constants.UPLOAD ~ 'product/s_' ~ f.image %}
                                {% else %}
                                    {% set src = constants.THEME ~ 'assets/images/products/no_photo_s.jpg' %}
                                {% endif %}
                                <div class="shopping-cart-table__product-image"><a href="{{ globals.uri }}product/detail/{{ f.product_id }}/{{ f.variant_id }}">
                                        <img src="{{ src }}" alt=""/>
                                    </a>
                                </div>
                            </td>
                            <td class="text-left">
                                <div class="th-title visible-xs">{{ f.title }}:</div>
                                <h6 class="shopping-cart-table__product-name text-left text-uppercase"><a href="{{ globals.uri }}product/detail/{{ f.product_id }}/{{ f.variant_id }}">{{ f.title }}</a>
                                </h6></td>
                            <td>
                                <div class="th-title visible-xs">{{ langs.price }}:</div>
                                <div class="shopping-cart-table__product-price">{{ f.price > 0 ? f.price ~ langs.valute_symbol : f.sale ~ langs.valute_symbol }}</div>
                            </td>
                            <td>
                                <div class="th-title visible-xs">{{ langs.delete }}:</div>
                                <a class="icon-clear remove-favorite" href="{{ globals.uri }}favorite/remove/{{ f.product_id }}" data-product-id="{{ f.product_id }}"></a>
                            </td>
                        </tr>
                    {% endfor %}
                    </tbody>
                </table>
            </div>
        </div>
        <div class="divider divider--lg"></div>
    </section>
    <!-- End Content section -->
{% endblock %}