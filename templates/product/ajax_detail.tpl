<div class="col-sm-5 col-md-5 col-lg-6">
    <div class="product-main-image product-container">
        <div class="product-main-image__item">
            {% set src = constants.THEME ~ 'assets/images/products/no_photo_l.jpg' %}
            {% if Images | length %}
                {% set src = constants.UPLOAD ~ 'product/m_' ~ Images[0].photo_name %}
                {% set big = constants.UPLOAD ~ 'product/l_' ~ Images[0].photo_name %}
            {% endif %}
            <img class="{{ Images | length ? 'product-zoom' }}" src='{{ src }}' data-zoom-image="{{ big }}"/>
        </div>
    </div>
    <div class="product-images-carousel">
        <ul id="smallGallery">
            {% for i in Images %}
                <li class="">
                    <a class="{{ loop.index == 1 ? 'active' }}" href="#" data-image="{{ constants.UPLOAD }}product/m_{{ i.photo_name }}" data-zoom-image="{{ constants.UPLOAD }}product/l_{{ i.photo_name }}">
                        <img src="{{ constants.UPLOAD }}product/s_{{ i.photo_name }}" alt=""/>
                    </a>
                </li>
            {% endfor %}
        </ul>
    </div>
</div>
<div class="product-info col-sm-7 col-md-7 col-lg-6">
    <input id="product-id" type="hidden" value="{{ Product.product_id }}">
    <div class="product-info__title">
        <h2>{{ Product.title }}</h2>
    </div>
    <div class="product-info__sku pull-right">&nbsp;&nbsp;
        {% if Product.variant_id and Variant.quantity > 0 %}
            <span class="label label-success">{{ langs.in_stock }}</span>
        {% elseif Product.variant_id == '' and Product.quantity %}
            <span class="label label-success">{{ langs.in_stock }}</span>
        {% else %}
            <span class="label label-danger">{{ langs.not_in_stock }}</span>
        {% endif %}
    </div>
    <div class="price-box product-info__price">
        {% if Product.price > 0 and Product.sale > 0 %}
            <span class="price-box__new">{{ Product.sale }} {{ langs.valute_symbol }} </span>
            <span class="price-box__old">{{ Product.price }} {{ langs.valute_symbol }}</span>
        {% elseif  Product.sale == 0 and Product.price > 0 %}
            <span class="price-box__new">{{ Product.price }} {{ langs.valute_symbol }}</span>
        {% endif %}
    </div>
    <div class="divider divider--xs product-info__divider"></div>
    {% if Product.brand %}
        <div class="product-info__description">{{ langs.brand }}: {{ Product.brand }}</div>
    {% endif %}
    {% if Product.code %}
        <div class="product-info__description">{{ langs.code }}: {{ Product.code }}</div>
    {% endif %}
    <div class="product-info__description">
        {{ Product.desc | raw }}
    </div>
    <div class="divider divider--xs product-info__divider"></div>
    {% for key, Attrs in Attr %}
        <div class="divider divider--xs"></div>
        <label>{{ key }}:</label>
        <ul class="options-swatch options-swatch--{{ Attrs[0].color ? 'color' : 'size' }} options-swatch--lg">
            {% for a in Attrs %}
                {% if a.color %}
                    <li class="{{ a.attr_value_id in VariantAttr ? 'active active-color' }}">
                        <a class="attr-value" href="" data-attr-id="{{ a.attr_value_id }}">
                            <div class="swatch-label" style="height:26px;width:28px;border:1px solid #ccc;background-color:{{ a.color }}"></div>
                        </a>
                    </li>
                {% else %}
                    <li class="{{ a.attr_value_id in VariantAttr ? 'active' }}" style="padding: 0 3px!important;width:auto;">
                        <a class="attr-value tooltip-link" href="" title="{{ a.title }}" data-attr-id="{{ a.attr_value_id }}">{{ a.title }}</a>
                    </li>
                {% endif %}
            {% endfor %}
        </ul>
    {% endfor %}
    <div class="divider divider--sm"></div>
    <label>{{ langs.quantity }}:</label>
    <div class="outer">
        <div class="input-group-qty pull-left"><span class="pull-left"> </span>
            <input type="text" name="quantity" class="input-number input--wd input-qty pull-left" value="1" min="1" max="{{ Product.quantity }}">
            <span class="pull-left btn-number-container">
            <button type="button" class="btn btn-number btn-number--plus" data-type="plus" data-field="quantity"> + </button>
            <button type="button" class="btn btn-number btn-number--minus" disabled data-type="minus" data-field="quantity"> –  </button>
            </span>
        </div>
        <div class="pull-left">
            <button class="btn btn--wd text-uppercase {% if Product.quantity <= 0 %}disabled{% else %}ajax-to-cart detail{% endif %}" data-product-id="{{ Product.product_id }}" data-variant-id="{{ Product.variant_id }}">{{ langs.cart_add }}</button>
        </div>
        <div class="social-links social-links--colorize social-links--invert social-links--padding pull-right">
            <span>{{ langs.share }}: </span>
            <div class="fb-share-button" data-href="{{ globals.uri }}product/detail/{{ Product.product_id }}/{{ Product.variant_id }}" data-layout="button_count" data-size="small" data-mobile-iframe="true">
                <a class="fb-xfbml-parse-ignore" target="_blank"></a>
            </div>
        </div>
    </div>
    <div class="divider divider--xs"></div>
    <ul class="product-links product-links--inline">
        <li class="save-to-favs"><a class="ajax-to-wishlist  {{ Product.product_id in FavoritesArray ? 'active' }}" data-product-id="{{ Product.product_id }}" href="#"><span class="icon icon-favorite"></span>{{ langs.save_product }}</a></li>
    </ul>
</div>