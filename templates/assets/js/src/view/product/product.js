$(function ($) {
    var action = false;
    $(document).on('click','.attr-value', function (e) {
        e.preventDefault();
        if (action)
            return false;
        if($(this).parent('li').hasClass('active'))
            return false;
        action = true;

        $(this).parents('ul').find('li.active').removeClass('active');
        $(this).parent('li').addClass('active');

        var attrs = [];
        $.each($('.options-swatch li.active a'), function () {
            var attrId = $(this).data('attr-id');
            attrs.push(attrId);
        });
        var productId = $('#product-id').val();
        $(".product-container img").css({
            'opacity': '0',
        });
        $.ajax({
            url: URI + 'product/getvariantid/',
            type: 'POST',
            dataType: 'json',
            data: {
                ajax : true,
                product_id : productId,
                attrs : attrs
            },
            success: function(data) {
                if (data.Status){
                    $('#ajax-content').html(data.Data.html);
                    var url = URI + 'product/detail/' + productId + '/' + data.Data.variant_id;
                    window.history.pushState({"html":data.Data.html,"pageTitle":'', "url": url, 'type': 'detail'},"", url);
                    action = false;
                    FB.XFBML.parse();
                    initDetailSlider();
                    initDetailZoom();
                    initImages();
                    $('.tooltip-link').tooltip();
                }
            }
        });
    });
});