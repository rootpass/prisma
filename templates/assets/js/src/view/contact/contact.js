$(function ($) {
    $('#contactform').validate({
        rules: {
            name: {
                required: true,
                minlength: 2
            },
            message: {
                required: true,
                minlength: 10
            },
            email: {
                required: true,
                email: true
            }

        },
        messages: {
            name: {
                required: lang.validate_name,
                minlength: lang.validate_name_len
            },
            message: {
                required: lang.validate_message,
                minlength: lang.validate_message_len
            },
            email: {
                required: lang.validate_email
            }
        },
        submitHandler: function(form) {
            $(form).ajaxSubmit({
                type: "POST",
                data: $(form).serialize(),
                dataType: 'json',
                url: URI + 'contact/send',
                success: function(data) {
                    if(data.Status){
                        $('#success').fadeIn();
                        $('#contactform').each(function() { this.reset(); });
                    }else{
                        $('#contactform').fadeTo("slow", 0, function() {
                            $('#error').fadeIn();
                        });
                    }
                },
                error: function() {
                    $('#contactform').fadeTo("slow", 0, function() {
                        $('#error').fadeIn();
                    });
                }
            });
        }
    });
});