var $ = jQuery.noConflict();

// Set Product Size
function debouncer(func, timeout) {
    var timeoutID, timeout = timeout || 500;
    return function () {
        var scope = this,
            args = arguments;
        clearTimeout(timeoutID);
        timeoutID = setTimeout(function () {
            func.apply(scope, Array.prototype.slice.call(args));
        }, timeout);
    }
}
// MENU HOVER
jQuery(function ($) {

    "use strict";

    var windowW = window.innerWidth || $(window).width();

    function menuIni(windowW) {

        if (windowW > 768) {
            $('ul.navbar-nav > li').addClass('hovernav');
            $('ul.navbar-nav > li.hovernav').hover(
                function () {
                    $(this).addClass("open")
                },
                function () {
                    $(this).removeClass("open")
                }
            );
            $('ul.navbar-nav > li li').hover(
                function () {
                    if ($(this).children('ul.level-menu__dropdown').length) {
                        $(this).addClass('active');
                        $(this).children('ul.level-menu__dropdown').show().css({'opacity': 0, 'left': $(this).width()})
                        var off = $(this).children('ul.level-menu__dropdown').offset();
                        var l = off.left;
                        var w = $(this).children('ul.level-menu__dropdown').width();
                        var docW = $(".container").width();
                        var isEntirelyVisible = (l + w <= docW);

                        if (!isEntirelyVisible) {
                            $(this).children('ul.level-menu__dropdown').show().css({'opacity': 1, 'right': $(this).width(), 'left': 'auto'})
                        } else {
                            $(this).children('ul.level-menu__dropdown').show().css({'opacity': 1, 'left': $(this).width()})
                        }

                    }
                },
                function () {
                    if ($(this).children('ul.level-menu__dropdown').length) {
                        $(this).removeClass('active');
                        $(this).children('ul.level-menu__dropdown').hide().css({'left': 'auto', 'right': 'auto'})
                    }
                }
            );
        } else {
            $('ul.navbar-nav > li').removeClass('hovernav');
            $('ul.navbar-nav > li li').unbind('mouseenter mouseleave');
            $('.touch ul.navbar-nav > li > a').click(function (e) {
                e.preventDefault();
            })
            $('.touch ul.navbar-nav > li a span.caret').click(function () {
                $(this).parent().parent('li').toggleClass('open');
            });
            $('.touch ul.navbar-nav > li a span.link-name').click(function () {
                var url = $(this).parent('a').attr("href");
                window.location = url;
            });

        }
        ;

    }

    menuIni(windowW);

    $('.no-touch .hovernav a').click(function () {
        window.location = this.href;
    });

    var prevW = windowW;

    $(window).resize(debouncer(function (e) {

        var currentW = window.innerWidth || $(window).width();
        if (currentW != prevW) {
            // start resize events					
            menuIni(currentW);
            // end resize events		
        }
        prevW = window.innerWidth || $(window).width();
    }));


});


// Price Slider initialize
jQuery(function ($) {
    "use strict";
    initImages();
    initPriceSlider();
    initDetailSlider();
    initDetailZoom();
    initSelectPicker();
    initCountDown();
    initImages();
});

function initImages() {
    $(".product-container").imagesLoaded(function() {
        $(".product-container img").each(function() {
            $(this).css({
                'opacity': '1',
            });
        });
    });
}

function initCountDown() {
    //if ($("#countdown1").length > 0) { $('#countdown1').countdown({ until: new Date(2016, 02, 1) }); }
    $('.countdown').each(function () {
        var $this = $(this), stringDate = $(this).data('countdown');
        var date = new Date(stringDate);
        var lang = $('html').attr('lang');
        $.countdown.setDefaults($.countdown.regionalOptions[lang]);
        $this.countdown({
            until: date,
        });
    });
}
function initSelectPicker() {
    if ($('.selectpicker').length) {
        $('.selectpicker').selectpicker({
            showSubtext: true
        });
    }
}

function initPriceSlider() {
    if ($('.price-slider').length) {

        var priceSlider = document.getElementById('priceSlider');
        var priceMin = parseInt($('#priceMin').val());
        var priceMax = parseInt($('#priceMax').val());
        var priceFrom = parseInt($('#priceFrom').val());
        var priceTo = parseInt($('#priceTo').val());
        if (priceMin > priceFrom)
            priceMin = priceFrom;
        if (priceMax < priceTo)
            priceMax = priceTo;
        if(priceMax == 0){
            return false;
        }
        if(priceMin == priceMax){
            priceMin = 0;
        }

        console.log(priceFrom, priceTo, priceMin, priceMax);
        noUiSlider.create(priceSlider, {
            start: [priceFrom, priceTo],
            connect: true,
            step: 1,
            range: {
                'min': priceMin,
                'max': priceMax
            }
        });
        var tipHandles = priceSlider.getElementsByClassName('noUi-handle'),
            tooltips = [];

        // Add divs to the slider handles.
        for (var i = 0; i < tipHandles.length; i++) {
            tooltips[i] = document.createElement('div');
            tipHandles[i].appendChild(tooltips[i]);
        }

        tooltips[0].className += 'tooltip top';
        tooltips[0].innerHTML = '<div class="tooltip-inner"><span></span><div class="tooltip-arrow"></div></div>';
        tooltips[0] = tooltips[0].getElementsByTagName('span')[0];
        tooltips[1].className += 'tooltip top';
        tooltips[1].innerHTML = '<div class="tooltip-inner"><span></span><div class="tooltip-arrow"></div></div>';
        tooltips[1] = tooltips[1].getElementsByTagName('span')[0];

        // When the slider changes, write the value to the tooltips.
        priceSlider.noUiSlider.on('update', function (values, handle) {
            tooltips[handle].innerHTML = Math.round(values[handle]);
        });
        priceSlider.noUiSlider.on('change', function (values, handle) {
            $('#priceFrom').val(values[0]);
            $('#priceTo').val(values[1]);
            $('#priceTo').trigger('change');
        });
    }
}

function initDetailSlider() {
    // Product thumbnails carousel Start
    $('.product-images-carousel ul').slick({
        infinite: false,
        dots: false,
        slidesToShow: 5,
        slidesToScroll: 1,
        responsive: [{
            breakpoint: 1200,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 1
            }
        }, {
            breakpoint: 992,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 1
            }
        }]
    });
    // Product thumbnails carousel End
}
function initDetailZoom() {
    var windowW = window.innerWidth || document.documentElement.clientWidth;
    $('.product-zoom').imagesLoaded(function () {
        if ($('.product-zoom').length) {
            var zoomPosition
            if ($('html').css('direction').toLowerCase() == 'rtl') {
                zoomPosition = 11;
            } else {
                zoomPosition = 1
            }
            if (windowW > 767) {
                $('.product-zoom').elevateZoom({
                    zoomWindowHeight: $('.product-zoom').height(),
                    gallery: "smallGallery",
                    galleryActiveClass: 'active',
                    zoomWindowPosition: zoomPosition
                })
            } else {
                $(".product-zoom").elevateZoom({
                    gallery: "smallGallery",
                    zoomType: "inner",
                    galleryActiveClass: 'active',
                    zoomWindowPosition: zoomPosition
                });
            }
        }
    })

    $('.product-main-image > .product-main-image__zoom ').bind('click', function () {
        galleryObj = [];
        current = 0;
        itemN = 0;

        if ($('#smallGallery').length) {
            $('#smallGallery li a').not('.video-link').each(function () {
                if ($(this).hasClass('active')) {
                    current = itemN;
                }
                itemN++;
                var src = $(this).data('zoom-image'),
                    type = 'image';
                image = {};
                image["src"] = src;
                image["type"] = type;

                galleryObj.push(image);
            });
        } else {
            itemN++;
            var src = $(this).parent().find('.product-zoom').data('zoom-image'),
                type = 'image';
            image = {};
            image["src"] = src;
            image["type"] = type;

            galleryObj.push(image);
        }

        $.magnificPopup.open({
            items: galleryObj,
            gallery: {
                enabled: true,
            }
        }, current);

    });

    var prevW = windowW;


    $(window).resize(debouncer(function (e) {
        var currentW = window.innerWidth || $(window).width();

        if (currentW != prevW) {
            // start resize events

            $('.zoomContainer').remove();
            $('.elevatezoom').removeData('elevateZoom');

            if ($('.product-zoom').length) {
                if (currentW > 767) {
                    $('.product-zoom').elevateZoom({
                        zoomWindowHeight: $('.product-zoom').height(),
                        gallery: "smallGallery"
                    })
                } else {
                    $(".product-zoom").elevateZoom({
                        gallery: "smallGallery",
                        zoomType: "inner"
                    });
                }
            }
            // end resize events
        }
        prevW = window.innerWidth || $(window).width();
    }));
}

// Back to top button
jQuery(function ($) {

    "use strict";

    var windowH = $(window).height();
    var backPos;
    if ($("footer .back-to-top").length) {
        var backPos = $("footer .back-to-top").offset();
        if (backPos.top < windowH) {
            $("footer .back-to-top").hide();
        }
    }

    $("a[href='#top']").click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, "slow");
        return false;
    });
});


// If Header line not exists

jQuery(function ($) {

    "use strict";
    if (!$('.header-line').length) {
        //$('.header').addClass('header--no-line');
    }

});

// Categories menu mobile
jQuery(function ($) {

    "use strict";

    var windowW = window.innerWidth || $(window).width();

    $("#categoriesMenu").click(function () {
        $(".navbar-nav--vertical").slideToggle(250, function () {
            $("#categoriesMenu").toggleClass('open');
            $(".navbar-nav--vertical").toggleClass('open')
        });
        return false;
    });

})
// Slide menu mobile

jQuery(function($j) {

    "use strict";

    var windowW = window.innerWidth || $j(window).width();

    if (windowW < 768) {
        $j('#slidemenu').css({
            'height': $j(window).height()
        });
    }
    var toggler = '.navbar-toggle';
    var pagewrapper = '#pageContent';
    var footer = '.footer';
    var navigationwrapper = '.navbar-header';
    var menuwidth = '100%';
    var slidewidth = '80%';
    var menuneg = '-100%';

    $j("#slidemenu .slidemenu-close").on("click", function(e) {

        $j('body').removeClass('modal-open');

        if ( $j('html').css('direction').toLowerCase() == 'rtl' ) {
            $j('#slidemenu').stop().animate({
                right: menuneg
            });
        } else {
            $j('#slidemenu').stop().animate({
                left: menuneg
            });
        }
    });

    $j("#navbar").on("click", toggler, function(e) {

        $j('body').addClass('modal-open');

        var selected = $j(this).hasClass('slide-active');


        if ( $j('html').css('direction').toLowerCase() == 'rtl' ) {
            $j('#slidemenu').stop().show().animate({
                right: selected ? menuneg : '0px'
            });
        } else {
            $j('#slidemenu').stop().show().animate({
                left: selected ? menuneg : '0px'
            });
        }

    });

    var windowW = window.innerWidth || $j(window).width();

    var   prevW = windowW;


    $j(window).resize(function() {
        var currentW = window.innerWidth || $j(window).width();

        if (currentW != prevW) {
            // start resize events
            if (currentW > 767) {
                $j('#slidemenu').css({
                    'height': ''
                });
                $j('body').removeClass('modal-open');
                $j('#slidemenu').stop().animate({
                    left: menuneg
                });
            } else {
                $j('#slidemenu').css({
                    'height': $j(window).height()
                });
            }

            // end resize events
        }
        prevW = window.innerWidth || $j(window).width();


    });
});

// Listing page - mobile version - Filters slide

jQuery(function ($) {

    "use strict";

    var windowW = window.innerWidth || $(window).width();

    if (windowW < 768) {
        $('#filtersCol').css({
            'height': $(window).height()
        });
    }

    var filterneg = '-100%';

    $(document).on("click", "#showFilterMobile", function () {

        var active = $('#filtersCol').hasClass('filter-active');

        $('body').toggleClass('modal-open');

        $('#filtersCol').stop().animate({
            right: active ? filterneg : '0px'
        });

        $('#filtersCol').toggleClass('filter-active');

    });

    $(document).on("click", "#filtersColClose", function () {

        $('#filtersCol').stop().animate({
            right: filterneg
        });

        $('body').removeClass('modal-open');

        $('#filtersCol').toggleClass('filter-active');

    });

});


// Without zoom previews switcher
jQuery(function ($) {


    if (!$('.product-zoom').length) {

        $('#mainProductImg').css({'min-height': $('#mainProductImg img').height(), 'min-width': $('#mainProductImg img').width()})


        $('#smallGallery a').click(function (e) {
            e.preventDefault();
            $('#smallGallery a').removeClass('active');
            $(this).addClass('active');
            var targ = $(this).parent('li').index();
            var curImg = $('#mainProductImg').find('div.product-main-image__item.active');
            var cur = curImg.index();
            if (targ == cur) {
                return false;
            } else {
                var newImg = $('#mainProductImg').find('div.product-main-image__item:nth-child(' + (targ + 1) + ')');
                curImg.removeClass('active');
                newImg.addClass('active')
            }
        })

    }

    var prevW = window.innerWidth || $(window).width();

    $(window).resize(debouncer(function (e) {

        var currentW = window.innerWidth || $(window).width();
        if (currentW != prevW) {
            // start resize events						
            if (!$('.product-zoom').length) {

                $('#mainProductImg').css({'min-height': '', 'min-width': ''})
                $('#mainProductImg').css({'min-height': $('#mainProductImg img').height(), 'min-width': $('#mainProductImg img').width()})

            }
            // end resize events		
        }
        prevW = window.innerWidth || $(window).width();
    }));

})


// Sticky Header
jQuery(function ($) {

    "use strict";

    if ($('header').length) {
        if ($(document).scrollTop() > 150) {
            $('header .navbar').addClass('stuck--smaller');
            $('.logo-default').addClass('smaller');
        }

        if ($('header.header--max.header--sticky').length) {
            var sticky = new Waypoint.Sticky({
                element: $('header #slidemenu')[0],
                offset: -1
            })
        } else {
            if ($('header.header--sticky .navbar').length) {
                var $toSticky = $('.header__dropdowns-container');
                var sticky = new Waypoint.Sticky({
                    element: $('header .navbar')[0],
                    offset: -1,
                    handler: function (direction) {
                        if (direction == 'down') {
                            var $toStickyCopy = $toSticky.detach();
                            $toStickyCopy.appendTo("#navbar");
                        } else {
                            var $toStickyCopy = $toSticky.detach();
                            $toStickyCopy.insertBefore(".sticky-wrapper");
                        }
                    }
                })
            }
        }
    }
    ;

    $("body").on("touchend", function () {
        if ($(document).scrollTop() > 150) {
            if (!$('header .navbar').hasClass('stuck--smaller')) {
                setTimeout(function () {
                    $('header .navbar').addClass('stuck--smaller');
                }, 300);
            }
        } else {
            $('header .navbar').removeClass('stuck--smaller');
        }
    });


    var waypoints = $('.no-touch header .navbar').waypoint(function (direction) {
        if (direction === 'down') {
            $('.no-touch header .navbar').addClass('stuck--smaller');
            $('.logo-default').addClass('smaller');
        }
    }, {
        offset: -350
    })
    var waypoints = $('.no-touch header .navbar').waypoint(function (direction) {
        if (direction === 'up') {
            $('.no-touch header .navbar').removeClass('stuck--smaller');
            $('.logo-default').removeClass('smaller');
        }
    }, {
        offset: -350
    })

    var prevW = window.innerWidth || $(window).width();

    $(window).resize(function () {
        var currentW = window.innerWidth || $(window).width();

        if (currentW != prevW) {
            // start resize events
            if ($(document).scrollTop() > 50) {
                $('header .navbar').addClass('stuck--smaller');
                $('.logo-default').addClass('smaller');
            }
            // end resize events
        }

        prevW = window.innerWidth || $(window).width();

    });


});


// Mobile footer collapse

jQuery(function ($) {

    "use strict";

    $('.mobile-collapse__title').click(function (e) {
        e.preventDefault;
        $(this).parent('.mobile-collapse').toggleClass('open');
    })
});

// Filter collapse

jQuery(function ($) {

    "use strict";

    $(document).on('click', '.filters-col__collapse__title', function (e) {
        e.preventDefault;
        $(this).parent('.filters-col__collapse').toggleClass('open');
    })

});

// Box collapse - shopping cart

jQuery(function ($) {

    "use strict";

    $('.card--collapse .card-title').click(function (e) {
        e.preventDefault;
        $(this).parent('.card').toggleClass('open');
    })


});

jQuery(function ($) {

    "use strict";

    // Product carousel Start
    $('.product-carousel.four-in-row').slick({
        infinite: true,
        dots: false,
        lazyLoad: 'ondemand',
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [{
            breakpoint: 992,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3
            }
        }, {
            breakpoint: 768,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        }, {
            breakpoint: 560,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }, {
            breakpoint: 321,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                centerMode: true,
                centerPadding: '20px'
            }
        }]
    });

    // Category carousel Start
    $('.product-category-carousel').slick({
        infinite: true,
        dots: false,
        lazyLoad: 'ondemand',
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        responsive: [{
            breakpoint: 992,
            settings: {
                slidesToShow: 4,
                slidesToScroll: 4
            }
        }, {
            breakpoint: 768,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3
            }
        }, {
            breakpoint: 560,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                centerMode: true,
                centerPadding: '40px'
            }
        }]
    });
    // Category carousel End

    // Category carousel Start
    $('.product-category-carousel-aside').slick({
        infinite: true,
        dots: false,
        lazyLoad: 'ondemand',
        slidesToShow: 4,
        slidesToScroll: 4,
        responsive: [{
            breakpoint: 992,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3
            }
        }, {
            breakpoint: 768,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        }, {
            breakpoint: 560,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                centerMode: true,
                centerPadding: '40px'
            }
        }]
    });
    // Category carousel End

    // Brand carousel Start
    if ($('.brands-carousel').closest('.aside-column').length) {
        $('.brands-carousel').slick({
            infinite: true,
            dots: false,
            lazyLoad: 'ondemand',
            slidesToShow: 5,
            slidesToScroll: 5,
            responsive: [{
                breakpoint: 992,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4
                }
            }, {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            }, {
                breakpoint: 560,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            }, {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    centerMode: true,
                    centerPadding: '50px'
                }
            }]
        });
    } else {
        $('.brands-carousel').slick({
            infinite: true,
            dots: false,
            lazyLoad: 'ondemand',
            slidesToShow: 7,
            slidesToScroll: 7,
            responsive: [{
                breakpoint: 992,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4
                }
            }, {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3
                }
            }, {
                breakpoint: 560,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            }, {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    centerMode: true,
                    centerPadding: '50px'
                }
            }]
        });
    }
    // Brand carousel End

    // Blog carousel Start
    var blogCarousel = $('.blog-carousel');

    if (blogCarousel.hasClass('show-2')) {
        blogCarousel.slick({
            infinite: true,
            dots: false,
            slidesToShow: 2,
            slidesToScroll: 2,
            responsive: [{
                breakpoint: 768,
                settings: {
                    dots: true
                }
            }, {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }]
        });
    } else if (blogCarousel.hasClass('show-3')) {
        blogCarousel.slick({
            infinite: true,
            dots: false,
            slidesToShow: 3,
            slidesToScroll: 2,
            responsive: [{
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    dots: true
                }
            }, {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }]
        });
    } else if (blogCarousel.hasClass('show-4')) {
        blogCarousel.slick({
            infinite: true,
            dots: false,
            slidesToShow: 4,
            slidesToScroll: 2,
            responsive: [{
                breakpoint: 1200,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 2,
                    dots: true
                }
            }, {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    dots: true
                }
            }, {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }]
        });
    } else {
        blogCarousel.slick({
            infinite: true,
            dots: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            responsive: [{
                breakpoint: 768,
                settings: {
                    dots: true
                }
            }]
        });
    }
    // Blog carousel End

    // Product mobile slider Start
    $('#singleGallery').slick({
        infinite: false,
        dots: true,
        arrows: false,
        slidesToShow: 1,
        slidesToScroll: 1
    });
    // Product mobile slider End

    // Simple slider Start
    $(".single-slider").imagesLoaded(function() {
        $('.single-slider img').css({
            'opacity': '1',
        });
    });

    $('.single-slider > ul').slick({
        infinite: true,
        dots: false,
        arrows: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 3000,
        speed: 1000,
        responsive: [{
            breakpoint: 768,
            settings: {
                arrows: false,
                dots: true
            }
        }]
    });
    // Simple slider End

});

// Parallax

jQuery(function ($) {

    "use strict";
    if ($('.content--parallax').length) {
        $('.content--parallax').each(function () {
            var attr = $(this).attr('data-image');
            $(this).css({'background-image': 'url(' + attr + ')'}).parallax("50%", 0.01);
        })
    }

    if ($('.parallax').length) {
        $('.parallax').each(function () {
            var attr = $(this).attr('data-image');
            $(this).css({'background-image': 'url(' + attr + ')'}).parallax("50%", 0);
        })
    }

});

// Image background

jQuery(function ($) {

    "use strict";
    if ($('.image-bg').length) {
        $('.image-bg').each(function () {
            var attr = $(this).attr('data-image');
            $(this).css({'background-image': 'url(' + attr + ')'});
        })
    }

});

jQuery(function ($) {

    "use strict";

    $('#productOther > li').hover(function () {
        $(this).toggleClass('show-image');
    });

});

jQuery(function ($) {

    "use strict";

    $('.header__search input').focus(function () {

        $('.search-focus-fade').stop().animate({
            "opacity": "0"
        }, 200);

    }).blur(function () {

        $('.search-focus-fade').stop().animate({
            "opacity": "1"
        }, 200);

    });

})


jQuery(function ($) {

    "use strict";

    function onScrollInit(items, container) {
        items.each(function () {
            var element = $(this),
                animationClass = element.attr('data-animation'),
                animationDelay = element.attr('data-animation-delay');

            element.css({
                '-webkit-animation-delay': animationDelay,
                '-moz-animation-delay': animationDelay,
                'animation-delay': animationDelay
            });

            var trigger = (container) ? container : element;

            trigger.waypoint(function () {
                element.addClass('animated').addClass(animationClass);
            }, {
                triggerOnce: true,
                offset: '90%'
            });
        });
    }

    onScrollInit($('.animation'));
    onScrollInit($('.staggered-animation'), $('.staggered-animation-container'));
});

// Blog Post button extend

jQuery(function ($) {

    "use strict";

    $('.btn-plus > a').on('click', function (e) {
        e.preventDefault() // prevent default action - hash doesn't appear in url
        $(this).parent().find('div').toggleClass('btn-plus__links--active');
        $(this).toggleClass('expanded');
    });

});

// tooltip ini

jQuery(function ($) {

    "use strict";
    $('.tooltip-link').tooltip();

});

// open menu button small Header

jQuery(function ($) {

    "use strict";
    $('#openMenu').on('click', function (e) {

    })

});


/*!
 * classie - class helper functions
 * from bonzo https://github.com/ded/bonzo
 * 
 * classie.has( elem, 'my-class' ) -> true/false
 * classie.add( elem, 'my-new-class' )
 * classie.remove( elem, 'my-unwanted-class' )
 * classie.toggle( elem, 'my-class' )
 */

/*jshint browser: true, strict: true, undef: true */
/*global define: false */

(function (window) {

    'use strict';

    // class helper functions from bonzo https://github.com/ded/bonzo

    function classReg(className) {
        return new RegExp("(^|\\s+)" + className + "(\\s+|$)");
    }

    // classList support for class management
    // altho to be fair, the api sucks because it won't accept multiple classes at once
    var hasClass, addClass, removeClass;

    if ('classList' in document.documentElement) {
        hasClass = function (elem, c) {
            return elem.classList.contains(c);
        };
        addClass = function (elem, c) {
            elem.classList.add(c);
        };
        removeClass = function (elem, c) {
            elem.classList.remove(c);
        };
    } else {
        hasClass = function (elem, c) {
            return classReg(c).test(elem.className);
        };
        addClass = function (elem, c) {
            if (!hasClass(elem, c)) {
                elem.className = elem.className + ' ' + c;
            }
        };
        removeClass = function (elem, c) {
            elem.className = elem.className.replace(classReg(c), ' ');
        };
    }

    function toggleClass(elem, c) {
        var fn = hasClass(elem, c) ? removeClass : addClass;
        fn(elem, c);
    }

    var classie = {
        // full names
        hasClass: hasClass,
        addClass: addClass,
        removeClass: removeClass,
        toggleClass: toggleClass,
        // short names
        has: hasClass,
        add: addClass,
        remove: removeClass,
        toggle: toggleClass
    };

    // transport
    if (typeof define === 'function' && define.amd) {
        // AMD
        define(classie);
    } else {
        // browser global
        window.classie = classie;
    }

})(window);


// Modal Search Popup


jQuery(function ($) {

    if ($('.overlay').length && $('.search-open').length) {

        var triggerBttn = $('.search-open'),
            overlay = document.querySelector('div.overlay'),
            closeBttn = overlay.querySelector('button.overlay-close');
        transEndEventNames = {
            'WebkitTransition': 'webkitTransitionEnd',
            'MozTransition': 'transitionend',
            'OTransition': 'oTransitionEnd',
            'msTransition': 'MSTransitionEnd',
            'transition': 'transitionend'
        },
            transEndEventName = transEndEventNames[Modernizr.prefixed('transition')],
            support = {
                transitions: Modernizr.csstransitions
            };

        function toggleOverlay() {
            if (classie.has(overlay, 'open')) {
                $('body').removeClass('modal-open');
                classie.remove(overlay, 'open');
                classie.add(overlay, 'close');
                var onEndTransitionFn = function (ev) {
                    if (support.transitions) {
                        if (ev.propertyName !== 'visibility') return;
                        this.removeEventListener(transEndEventName, onEndTransitionFn);
                    }
                    classie.remove(overlay, 'close');
                };
                if (support.transitions) {
                    overlay.addEventListener(transEndEventName, onEndTransitionFn);
                } else {
                    onEndTransitionFn();
                }
            } else if (!classie.has(overlay, 'close')) {
                classie.add(overlay, 'open');
                $('body').addClass('modal-open');
                $('input[name=word]').trigger('focus');
            }

            return false;
        }

        triggerBttn.on('click', toggleOverlay);
        closeBttn.addEventListener('click', toggleOverlay);
    }
});


// MD inputs label

jQuery(function ($) {

    "use strict";

    $('.input-group--wd > input, .input-group--wd > textarea ').blur(function () {
        var $this = $(this);
        if ($this.val())
            $this.addClass('used');
        else
            $this.removeClass('used');
    });

});

// Remove Loader

jQuery(function ($) {

    "use strict";
    $('body').addClass('loaded');

});


// Set carousel nav position

jQuery(function ($) {

    "use strict";

    var prevW = window.innerWidth || $(window).width();

    if ($('body').hasClass('boxed')) {
        $('.aside-column .slick-slider.slick-initialized').each(function () {
            if (!$(this).hasClass('nav-top') && !$(this).hasClass('nav-dot')) {
                $(this).addClass('nav-inside');
            }
        })
    }

    if ($('body').hasClass('fullwidth')) {
        $('.slick-slider.slick-initialized').each(function () {
            if (!$(this).hasClass('nav-top') && !$(this).hasClass('nav-dot')) {
                $(this).addClass('nav-inside');
            }
        })
    } else {
        $('.slick-slider.slick-initialized').each(function () {
            if (!$(this).closest('.single-slider').length) {

                if ($('body').hasClass('boxed') && !$(this).hasClass('blog-carousel')) {
                    var outerW = prevW;
                } else var outerW = $(this).closest('section').width();

                if ((outerW - $(this).width()) * 0.5 < 45) {
                    $(this).addClass('nav-inside');
                    $('.slick-initialized').slick('setPosition');
                }

            }
        });
    }

    $(window).resize(debouncer(function (e) {
        var currentW = window.innerWidth || $(window).width();
        if (currentW != prevW) {
            // start resize events		

            $('.slick-slider.slick-initialized').each(function () {
                if (!$(this).closest('.single-slider').length) {

                    if ($('body').hasClass('boxed') && !$(this).hasClass('blog-carousel')) {
                        var outerW = currentW;
                    } else var outerW = $(this).closest('section').width();

                    if ((outerW - $(this).width()) * 0.5 < 45) {
                        $(this).addClass('nav-inside');
                        $('.slick-initialized').slick('setPosition');
                    } else {
                        $(this).removeClass('nav-inside');
                    }

                }
            });

            // end resize events		
        }
        prevW = window.innerWidth || $(window).width();

    }));

});

// double click fo touch devices

jQuery(function ($) {

    "use strict";

    var windowW = window.innerWidth || $(window).width();

    $('.touch .category-block__list a').doubleTapToGo();
    if (windowW > 767) {
        $('.touch ul.navbar-nav > li').each(function () {
            if ($(this).find('a').hasClass('dropdown-toggle')) {
                $(this).doubleTapToGo()
            }
        })
    }
    $('.touch .product-preview').doubleTapToGo();

});

// Add active class to opened accordion panel

jQuery(function ($) {

    "use strict";

    $('.panel-group')
        .on('show.bs.collapse', function (e) {
            $(e.target).prev('.panel-heading').addClass('active');
        })
        .on('hide.bs.collapse', function (e) {
            $(e.target).prev('.panel-heading').removeClass('active');
        });

});