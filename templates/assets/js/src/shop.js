var action = false;
$(function () {
    if(typeof GeoKBD !== 'undefined')
        GeoKBD.map('order-form',['user_name'], 'geo');
    
    $(document).on('click', '.keyboard', function () {
        var checked = $('#geo').is(":checked");
        if(checked){
            $("#geo").prop("checked", false);
            $(this).text('EN');
        }
        else{
            $("#geo").prop("checked", true);
            $(this).text('KA');
        }
    })

    $(document).on('click', '.remove-favorite', function (e) {
        e.preventDefault();
        var productId = $(this).data('product-id');
        var url = $(this).attr('href');
        bootbox.confirm({
            message: lang.delete_confirm,
            buttons: {
                confirm: {
                    label: lang.yes,
                    className: 'btn-danger'
                },
                cancel: {
                    label: lang.no,
                    className: 'btn-success'
                }
            },
            callback: function (result) {
                if(result){
                    $.getJSON(url + '&ajax=true', function (data) {
                        if (data.Status){
                            if(data.Data.Count)
                                $('.favorite-count').text(data.Data.Count);
                            else
                                $('.favorite-count').text('');
                            $('#' + productId).remove();
                            if($('.favorite-tr').length == 0)
                                $('.cart-empty').removeClass('hidden');
                        }
                    });
                }
            }
        });
    });
    var action = false;
    $(document).on('click', '.ajax-to-wishlist', function(e) {
        e.preventDefault();
        if(action)
            return false;
        action = true;
        var el = $(this);
        var productId = el.data('product-id');
        var remove = el.hasClass('active');
        if(remove){
            $.getJSON(URI + 'favorite/remove/' + productId + '/?ajax=true', function (data) {
                if (data.Status){
                    if(data.Data.Count)
                        $('.favorite-count').text(data.Data.Count);
                    else
                        $('.favorite-count').text('');
                    el.toggleClass('active');
                    action = false;
                }
            });

        }else{
            $.getJSON(URI + 'favorite/save/' + productId + '/?ajax=true', function (data) {
                $('.favorite-count').text(data.Data.Count);
                $('.favorite-count').addClass('shake');
                setTimeout(function () {
                    $('.favorite-count').removeClass('shake');
                }, 500);
                el.toggleClass('active');
                action = false;
            });
        }
    });

    var action = false;
    $(document).on('click', '.ajax-to-cart', function(e) {
        e.preventDefault();
        if(action)
            return false;
        var el = $(this);
        var productId = el.data('product-id');
        var variantId = el.data('variant-id');
        if(el.hasClass('detail')){
            var quantity = parseInt($('input[name=quantity]').val());
        }else{
            var quantity = parseInt(el.data('quantity'));
            if(quantity == 0){
                bootbox.alert(lang.not_in_stock_message);
                return false;
            }else{
                quantity = 1;
            }
        }
        action = true;
        el.addClass('btn--wait');
        $.ajax({
            url: URI + 'cart/add/',
            type: 'POST',
            dataType: 'json',
            data: {
                ajax : true,
                product_id : productId,
                variant_id : variantId,
                quantity : quantity,
            },
            success: function(data) {
                if (data.Status){
                    $('.cart-count').text(data.Data.Count);
                    el.removeClass('btn--wait');
                    action = false;
                    $('.cart-count').addClass('shake');
                    setTimeout(function () {
                        $('.cart-count').removeClass('shake');
                        showCartModal();
                    }, 500);

                }
            }
        });
    });

    $(document).on('click', '.ajax-remove-cart', function(e) {
        e.preventDefault();
        var el = $(this);
        var productId = el.data('product-id');
        var variantId = el.data('variant-id');
        bootbox.confirm({
            message: lang.delete_confirm,
            buttons: {
                confirm: {
                    label: lang.yes,
                    className: 'btn-danger'
                },
                cancel: {
                    label: lang.no,
                    className: 'btn-success'
                }
            },
            callback: function (result) {
                if(result){
                    $.ajax({
                        url: URI + 'cart/remove/',
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            ajax : true,
                            product_id : productId,
                            variant_id : variantId
                        },
                        success: function(data) {
                            if (data.Status){
                                if(data.Data.Count)
                                    $('.cart-count').text(data.Data.Count);
                                else
                                    $('.cart-count').text('');
                                $('#' + productId + '_' + variantId).remove();
                                calculateCart();
                                if($('.total-price').length == 0){
                                    $('.cart-empty').removeClass('hidden');
                                }
                            }
                        }
                    });
                }
            }
        });
    });
    var action = false;
    $(document).on('click', '.ajax-get-cart:not("disabled")', function(e) {
        //e.preventDefault();
        if(action)
            return false;
        action = true;
        var el = $(this);
        el.addClass('disabled');
        $.ajax({
            url: URI + 'cart/getcart/',
            type: 'POST',
            dataType: 'html',
            data: {
                ajax : true
            },
            success: function(data) {
                $('.cart-content').html(data);
                el.removeClass('disabled');
                el.parents('.dropdown').addClass('open');
                action = false;
            }
        });
    });

    $(document).on('click', '.ajax-clear-cart', function(e) {
        e.preventDefault();
        var el = $(this);
        bootbox.confirm({
            message: lang.delete_confirm,
            buttons: {
                confirm: {
                    label: lang.yes,
                    className: 'btn-danger'
                },
                cancel: {
                    label: lang.no,
                    className: 'btn-success'
                }
            },
            callback: function (result) {
                if(result){
                    $.ajax({
                        url: URI + 'cart/clear/',
                        type: 'POST',
                        dataType: 'json',
                        data: {
                            ajax : true
                        },
                        success: function(data) {
                            if (data.Status){
                                clearCart();
                            }
                        }
                    });
                }
            }
        });
    });

    var action = false;
    $(document).on('change', 'input[name=quantity].cart', function(e) {
        e.preventDefault();
        if(action){
            return false;
        }
        var el = $(this);
        var productId = el.data('product-id');
        var variantId = el.data('variant-id');
        var price = parseFloat(el.data('price'));
        var quantity = parseInt(el.val());

        action = true;
        $.ajax({
            url: URI + 'cart/add/',
            type: 'POST',
            dataType: 'json',
            data: {
                ajax : true,
                product_id : productId,
                variant_id : variantId,
                quantity : quantity,
                update : true,
            },
            success: function(data) {
                if (data.Status){
                    el.parents('tr').find('.total-price').text(parseFloat(quantity*price).toFixed(2));
                    calculateCart();
                    action = false;
                }
            }
        });
    });

    $(document).on('change', '#shiping-location', function(e) {
        if($(this).val() == 0){
           $(this).addClass('alert-danger');
        }else{
            $(this).removeClass('alert-danger');
        }
        calculateCart();
    });

    $('#order-popup').on('show.bs.modal', function (e) {
        if($('.cart-tr').length == 0)
            return false;
        var shopingSelect = $('#shiping-location');
        var shipingType = shopingSelect.val();
        if(shipingType == 0){
            shopingSelect.addClass('alert-danger');
            shopingSelect.focus();
            return false;
        }
    });

    $('#order-popup').on('hidden.bs.modal', function (e) {
        //$(".modal-content").html("");
    });
    $('#order-success-popup').on('hidden.bs.modal', function (e) {
        location.href = URI;
    });
    var action = false;
    $(document).on('click', '.ajax-order-confirm', function (e) {
        e.preventDefault();
        if(action)
            return false;
        var nameInput = $('#user-name');
        var phoneInput = $('#user-phone');
        var emailInput = $('#user-email');
        var shipingType = $('#shiping-location').val();

        var name = nameInput.val();
        var phone = phoneInput.val();
        var email = emailInput.val();

        if(name == '' || name.length < 4){
            nameInput.addClass('alert-danger');
            nameInput.focus();
            return false;
        } else{
            nameInput.removeClass('alert-danger');
        }
        if(phone == '' || phone.length < 6){
            phoneInput.addClass('alert-danger');
            phoneInput.focus();
            return false;
        } else{
            phoneInput.removeClass('alert-danger');
        }
        if(!isEmail(email)){
            emailInput.addClass('alert-danger');
            emailInput.focus();
            return false;
        } else{
            emailInput.removeClass('alert-danger');
        }

        action = true;
        $.ajax({
            url: URI + 'cart/order/',
            type: 'POST',
            dataType: 'json',
            data: {
                ajax : true,
                user_first_name : name,
                user_phone : phone,
                user_email : email,
                shiping_type : shipingType,
            },
            success: function(data) {
                if (data.Status){
                    console.log(data);
                    clearCart();
                    $('#order-popup').modal("hide");

                    $('.order-id').text(data.Data.OrderId);
                    $('.shiping-date').text(data.Data.ShipingDate);
                    $('.order-amount').text(data.Data.OrderAmount);

                    $('#order-success-popup').modal("show");
                }else{
                    $('#order-error').removeClass('hidden');
                    setTimeout(function () {
                        $('#order-error').addClass('hidden');
                    }, 5000);
                }
                action = false;
            }
        });
    });

    $(document).on('click', '.btn-number', function(e) {
        e.preventDefault();
        if(action)
            return false;
        var type = $(this).attr('data-type');
        var input = $(this).closest('.input-group-qty').find('input.input-qty');
        var currentVal = parseInt(input.val());
        if (!isNaN(currentVal)) {
            if (type == 'minus') {
                if (currentVal > input.attr('min')) {
                    input.val(currentVal - 1).change();
                }
                if (parseInt(input.val()) == input.attr('min')) {
                    $(this).attr('disabled', true);
                }

            } else if (type == 'plus') {

                if (currentVal < input.attr('max')) {
                    input.val(currentVal + 1).change();
                }
                if (parseInt(input.val()) == input.attr('max')) {
                    $(this).attr('disabled', true);
                }
            }
        } else {
            input.val(0);
        }
    });
    $(document).on('focusin', '.input-number', function() {
        $(this).data('oldValue', $(this).val());
    });
    $(document).on('change', '.input-number', function() {
        var minValue = parseInt($(this).attr('min'));
        var maxValue = parseInt($(this).attr('max'));
        var valueCurrent = parseInt($(this).val());

        if (valueCurrent >= minValue) {
            $(this).closest('.input-group-qty').find(".btn-number[data-type='minus']").removeAttr('disabled')
        } else {
            //alert('Sorry, the minimum value was reached');
            $(this).val($(this).data('oldValue'));
        }
        if (valueCurrent <= maxValue) {
            $(this).closest('.input-group-qty').find(".btn-number[data-type='plus']").removeAttr('disabled')
        } else {
            //alert('Sorry, the maximum value was reached');
            $(this).val($(this).data('oldValue'));
        }


    });

    // validate search price
    $(document).on('keydown', '.numeric', function(e){
        var maxLength = $(this).data('max');
        //
        if (e.keyCode === 86 && (e.ctrlKey === true || e.metaKey === true) ) {
            setTimeout(function() {
                e.target.value = e.target.value.replace(/[^0-9]/g,'');
            }, 0);
            return;
        }
        // Allow: backspace, delete, tab, escape, enter
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
            // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: Ctrl+C, Command+C
            (e.keyCode === 67 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: Ctrl+X, Command+X
            (e.keyCode === 88 && (e.ctrlKey === true || e.metaKey === true)) ||
            // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
        if($(this).val().length == maxLength && this.selectionStart == maxLength)
            e.preventDefault();

    });

    $(document).on('paste', '.numeric', function(e){
        setTimeout(function() {
            e.target.value = e.target.value.replace(/[^0-9]/g,'');
        }, 0);
        return;
    });

    $(document).on('click', '.filter-checkbox', function(e) {
        var url = $(this).data('url');
        var resetUrl = $(this).data('reset-url');
        if($(this).is(':checked')){
            filterProducts(url);
        }else{
            filterProducts(resetUrl);
        }
    });

    $(document).on('change', '.sort-select', function(e) {
        var url = $(this).find('option:selected').val();
        filterProducts(url);
    });

    $(document).on('click', '.pagination a', function(e) {
        e.preventDefault();
        var url = $(this).attr('href');
        if(url)
            filterProducts(url);
    });

    $(document).on('change', '#priceFrom, #priceTo', function(e) {
        var priceFrom = parseInt($('#priceFrom').val(), 10);
        var priceTo = parseInt($('#priceTo').val(), 10);
        var url = $('#priceUrl').val() + '&price_from=' + priceFrom + '&price_to=' + priceTo;
        filterProducts(url);
    });

});

$(window).on('popstate', function (e) {
    var state = e.originalEvent.state;
    if (state.html !== null) {
        if(state.type == 'detail'){
            $('#ajax-content').html(state.html);
            initDetailSlider();
            initDetailZoom();
            initImages();
            $('.tooltip-link').tooltip();
            FB.XFBML.parse();
        }
        if(state.type == 'filter'){
            $('#ajax-content').html(state.html);
            initCountDown();
            initSelectPicker();
            initPriceSlider();
            initImages();
        }
    }
});

// After add cart show popup
function showCartModal() {
    bootbox.confirm({
        message: lang.continue_shoping_confirm,
        buttons: {
            confirm: {
                label: lang.no,
                className: 'btn-default'
            },
            cancel: {
                label: lang.yes,
                className: 'btn-default'
            }
        },
        callback: function (result) {
            console.log(result);
            if(result){
                location.href = URI + 'cart';
            }
        }
    });
}

// Calculate cart amount
function calculateCart() {
    var sum = 0;
    $('.total-price').each(function(){
        sum += parseFloat($(this).text());
    });
    var shipingAmount = 0;
    var shipingElement = $('#shiping-location option:selected');
    var shipingType = shipingElement.val();
    if(shipingType){
        var shipingPrice = parseFloat(shipingElement.data('price'));
        var shipingFree = shipingElement.data('free');
        if(shipingFree > sum){
            shipingAmount = shipingPrice;
            if(shipingType == 1){
                var requiredAmount = shipingFree-sum;
                $('.shiping-info').text($('.shiping-info').text().replace('{amount}', requiredAmount.toFixed(2))).removeClass('hidden');
            }else{
                $('.shiping-info').addClass('hidden');
            }
        }else{
            $('.shiping-info').addClass('hidden');
        }

    }
    var totalAmount = sum+shipingAmount;
    $('.shiping-amount').text(shipingAmount.toFixed(2));
    $('.total-amount').text(totalAmount.toFixed(2));
}

// Check Email
function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

function clearCart() {
    $('.cart-count').text('');
    $('.cart-tr').remove();
    $('.total-amount').text(0);
    $('.cart-empty').removeClass('hidden');
}

// After add cart show popup
function filterProducts(url) {
    if(action)
        return false;
    action = true;
    $('.ajax-loader').show();
    $.get(url + '&ajax=true', function (data) {
        if (data){
            $('.ajax-loader').hide();
            $('#ajax-content').html(data);
            window.history.pushState({"html":data,"pageTitle":'', "url": url, 'type': 'filter'},"", url);
            initCountDown();
            initSelectPicker();
            initPriceSlider();
            initImages();
            $('body').removeClass('modal-open');
            action = false;
        }
    });
}