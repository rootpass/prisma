<div class="shopping-cart__top text-uppercase">{{ langs.my_cart }} ({{ Count }})</div>
<ul>
    <div class="cart-empty {{ Products | length ? 'hidden' }} text-center"><h5>{{ langs.cart_empty }}</h5></div>
    {% for p in Products %}
        <li class="shopping-cart__item">
            <div class="shopping-cart__item__image pull-left">
                <a href="{{ globals.uri }}product/detail/{{ p.product_id }}/{{ p.variant_id }}">
                    {% if p.variant_photo_name %}
                        {% set src = constants.UPLOAD ~ 'product/s_' ~ p.variant_photo_name %}
                    {% elseif p.photo_name %}
                        {% set src = constants.UPLOAD ~ 'product/s_' ~ p.photo_name %}
                    {% else %}
                        {% set src = constants.THEME ~ 'assets/images/products/no_photo_s.jpg' %}
                    {% endif %}
                    <img src="{{ src }}" alt="">
                </a>
            </div>
            <div class="shopping-cart__item__info">
                <div class="shopping-cart__item__info__title"><h2 class="text-uppercase"><a href="#">{{ p.title }}</a></h2></div>
                {% set attrs = p.attr | split(',') %}
                {% for a in attrs %}
                    <div class="shopping-cart__item__info__option">{{ a }}</div>
                {% endfor %}
                <div class="shopping-cart__item__info__price">{{ p.sale > 0 ? p.sale : p.price }} {{ langs.valute_symbol }}</div>
                <div class="shopping-cart__item__info__qty">{{ langs.quantity }}:{{ p.quantity }}</div>
                <div class="shopping-cart__item__info__delete">
                    <a href="#" class="icon icon-clear ajax-remove-cart" data-product-id="{{ p.product_id }}" data-variant-id="{{ p.variant_id }}"></a>
                </div>
            </div>
        </li>
    {% endfor %}
</ul>
<div class="shopping-cart__bottom">
    <div class="pull-left">{{ langs.sum }}: <span class="shopping-cart__total">{{ Total }}</span> {{ langs.valute_symbol }}</div>
    <div class="pull-right">
        <button class="btn btn--wd text-uppercase {{ Products | length == 0 ? 'disabled' }}"><a href="{{ globals.uri }}cart">{{ langs.order }}</a></button>
    </div>
</div>