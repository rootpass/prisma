{% extends "index.tpl" %}

{% block content %}
    <section class="breadcrumbs  hidden-xs">
        <div class="container">
            <ol class="breadcrumb breadcrumb--wd pull-left">
                <li><a href="{{ globals.uri }}">{{ langs.main }}</a></li>
                <li class="active">{{ langs.saved_products }}</li>
            </ol>
        </div>
    </section>
    <!-- Content section -->
    <section class="content">
        <div class="modal quick-view zoom" id="order-popup" style="opacity: 1;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <button type="button" class="close icon-clear" data-dismiss="modal"></button>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-sm-12 container text-center row">
                                    <form name="order-form">
                                        <h3>{{ langs.order_confirm_header }}</h3>
                                        {% if langs.order_confirm_text %}
                                            <div class="alert alert-info">{{ langs.order_confirm_text }}</div>
                                            <div class="divider divider--xs"></div>
                                        {% endif %}
                                        <!-- Text input-->
                                        <div class="col-md-12 form-group">
                                            <label class="col-md-4 control-label" for="textinput">{{ langs.first_name }} {{ langs.last_name }}</label>
                                            <div class="col-md-4">
                                                <input id="user-name" name="user_name" type="text" placeholder="" value="{{ cookie.name }}" class="form-control input-md" maxlength="35">
                                                <input class="hidden" type="checkbox" id="geo" name="geo" checked="">
                                                <i class="keyboard">KA</i>
                                            </div>
                                        </div>
                                        <!-- Text input-->
                                        <div class="col-md-12 form-group">
                                            <label class="col-md-4 control-label" for="textinput">{{ langs.phone }}</label>
                                            <div class="col-md-4">
                                                <input id="user-phone" name="textinput" type="text" placeholder="" value="{{ cookie.phone }}" class="form-control input-md numeric" data-max="13">
                                            </div>
                                        </div>
                                        <!-- Text input-->
                                        <div class="col-md-12 form-group">
                                            <label class="col-md-4 control-label" for="textinput">{{ langs.email }}</label>
                                            <div class="col-md-4">
                                                <input id="user-email" name="textinput" type="text" placeholder="" value="{{ cookie.email }}" class="form-control input-md">
                                            </div>
                                        </div>
                                        <div class="col-md-12 form-group">
                                            <a href="#" class="btn btn--wd ajax-order-confirm">{{ langs.order_confirm }}</a>
                                        </div>
                                        <div class="col-md-12 text-center">
                                            <div id="order-success" class="alert alert-success hidden">
                                                {{ langs.order_send_success }}
                                            </div>
                                            <div  id="order-error" class="alert alert-danger hidden">
                                                {{ langs.order_send_error }}
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal quick-view zoom" id="order-success-popup" style="opacity: 1;">
            <div class="modal-dialog">
                <div class="modal-content">
                   <div class="modal-body">
                       <button type="button" class="close icon-clear" data-dismiss="modal"></button>
                       <div class="container-fluid">
                           <div class="row">
                               <div class="col-sm-12 container text-center">
                                   <h2>{{ langs.congratulations }}!</h2>
                                   <p>{{ langs.order_success_message }}</p>
                                   <div>{{ langs.order_number }} : #<span class="order-id"></span></div>
                                   <div>{{ langs.shiping_date }} : <span class="shiping-date"></span></div>
                                   <div>{{ langs.amount }} : <span class="order-amount"></span> ₾</div>
                               </div>
                           </div>
                       </div>
                   </div>
                </div>
            </div>
        </div>
        <div class="container">
            <h2 class="text-uppercase text-xs-center">{{ langs.cart }}</h2>
            <div class="card card--padding">
                <table class="table shopping-cart-table text-center">
                    <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th>{{ langs.product_name }}</th>
                        <th>&nbsp;</th>
                        <th>{{ langs.price }}</th>
                        <th>{{ langs.quantity }}</th>
                        <th>{{ langs.total_amount }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="cart-empty {{ Products | length ? 'hidden' }} text-center text-xs-center">
                        <td colspan="6" class="text-center"><h5>{{ langs.cart_empty }}</h5></td>
                    </tr>
                    {% for p in Products %}
                        <tr id="{{ p.product_id }}_{{ p.variant_id }}" class="cart-tr">
                            <td>
                                <div class="th-title visible-xs">{{ langs.delete }}:</div>
                                <a class="icon-clear shopping-cart-table__delete ajax-remove-cart" href="#" data-product-id="{{ p.product_id }}" data-variant-id="{{ p.variant_id }}"></a></td>
                            <td class="no-title">
                                <div class="shopping-cart-table__product-image">
                                    <a href="{{ globals.uri }}product/detail/{{ p.product_id }}/{{ p.variant_id }}">
                                        {% if p.variant_photo_name %}
                                            {% set src = constants.UPLOAD ~ 'product/s_' ~ p.variant_photo_name %}
                                        {% elseif p.photo_name %}
                                            {% set src = constants.UPLOAD ~ 'product/s_' ~ p.photo_name %}
                                        {% else %}
                                            {% set src = constants.THEME ~ 'assets/images/products/no_photo_s.jpg' %}
                                        {% endif %}
                                        <img src="{{ src }}" alt=""/>
                                    </a>
                                </div>
                            </td>
                            <td>
                                <div class="th-title visible-xs">{{ langs.product_name }}:</div>
                                <h6 class="shopping-cart-table__product-name text-left text-uppercase">
                                    <a href="{{ globals.uri }}product/detail/{{ p.product_id }}/{{ p.variant_id }}">{{ p.title }}</a>
                                </h6>
                                <div class="shopping-cart-table__product-color text-left">
                                    {{ p.attr }}
                                </div>
                            </td>
                            <td>
                                <div class="th-title visible-xs">{{ langs.price }}:</div>
                                <div class="shopping-cart-table__product-price">{{ p.sale > 0 ? p.sale : p.price }} {{ langs.valute_symbol }}</div>
                            </td>
                            <td>
                                <div class="th-title visible-xs">{{ langs.quantity }}:</div>
                                <div class="input-group-qty text-center">
                                    <input type="text" name="quantity" class="cart input-number input--wd input-qty pull-left" value="{{ p.quantity }}"
                                           min="1" max="{{ p.max_quantity }}" data-product-id="{{ p.product_id }}" data-variant-id="{{ p.variant_id }}" data-price="{{ p.sale > 0 ? p.sale : p.price }}">
                                    <span class="pull-left btn-number-container">
                                    <button type="button" class="btn btn-number btn-number--plus" {{ p.quantity == p.max_quantity ? 'disabled' }} data-type="plus" data-field="quantity"> + </button>
                                    <button type="button" class="btn btn-number btn-number--minus" {{ p.quantity == 1 ? 'disabled' }} data-type="minus" data-field="quantity"> – </button>
                                </span>
                                </div>
                            <td>
                                <div class="th-title visible-xs">{{ langs.total_amount }}:</div>
                                <div class="shopping-cart-table__product-price"><span class="total-price">{{ p.sale > 0 ? p.sale*p.quantity : p.price*p.quantity }}</span> {{ langs.valute_symbol }}</div>
                            </td>
                        </tr>
                    {% endfor %}
                    </tbody>
                </table>
                <div class="hr"></div>
                <div class="divider divider--xs"></div>
                <div class="row shopping-cart-btns">
                    <div class="col-sm-4 col-md-4"><a href="{{ globals.uri }}" class="btn btn--wd pull-left">{{ langs.continue_shoping }}</a></div>
                    <div class="divider divider--xs visible-xs"></div>
                    <div class="col-sm-8 col-md-8">
                        <a href="#" class="btn btn--wd btn--light pull-right ajax-clear-cart">{{ langs.clear_cart }}</a>
                    </div>
                </div>
                <div class="divider divider--xxs"></div>
            </div>
            <div class="divider divider--xs"></div>
            <div class="row">
                <div class="col-xs-6 col-sm-4 col-md-3">
                    <h5>{{ langs.shiping_price }}</h5>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-4">
                    <h4><span class="shiping-amount">0</span> {{ langs.valute_symbol }}</h4>
                </div>
                <div class="col-md-2 hidden-sm"></div>
                <div class="col-xs-12 col-sm-4 col-md-3 text-right">
                    <div class="form-group">
                        <select class="form-control col-xs-12" id="shiping-location">
                            <option value="0">{{ langs.location }}</option>
                            <option value="1" data-price="{{ constants.SHIPING_TBILSI_PRICE }}" data-free="{{ constants.SHIPING_TBILSI_FREE }}">{{ langs.tbilisi }}</option>
                            <option value="2" data-price="{{ constants.SHIPING_REGION_PRICE }}" data-free="{{ constants.SHIPING_REGION_FREE }}">{{ langs.region }}</option>
                        </select>
                    </div>

                </div>
            </div>
            <div class="divider divider--xs"></div>
            <div class="alert alert-warning shiping-info hidden">
                {{ langs.shiping_info }}
            </div>
            <div class="divider divider--sm"></div>
            <div class="row">
                <div class="col-sm-4 col-md-3 col-xs-6">
                    <h3>{{ langs.total_amount }}</h3>
                </div>
                <div class="col-sm-4 col-md-2 col-xs-6 text-xs-center text-md-left">
                    <h2><span class="total-amount">{{ Total }}</span> {{ langs.valute_symbol }}</h2>
                </div>
                <div class="col-sm-4 col-md-7 col-xs-12 text-md-right text-xs-center">
                    <a href="#" class="btn btn--wd ajax-order" data-toggle="modal" data-target="#order-popup" data-dismiss="modal">{{ langs.order_continue }}</a>
                    <div class="divider divider--xs"></div>
                </div>
            </div>
        </div>
        <div class="divider divider--lg"></div>
    </section>
    <!-- End Content section -->
{% endblock %}

{% block footer %}
    <script src="{{ constants.THEME }}assets/js/build/geo_kbd.min.js{{ constants.ver }}"></script>
{% endblock %}