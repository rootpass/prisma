{% extends "index.tpl" %}

{% block head %}
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&key=AIzaSyAbFuepJJ9CxjpIwK8A8OW9kxvL9OWm9Ts"></script>
{% endblock %}

{% block content %}
    <!-- Breadcrumb section -->
    <section class="breadcrumbs  hidden-xs">
        <div class="container">
            <ol class="breadcrumb breadcrumb--wd pull-left">
                <li><a href="{{ globals.uri }}">{{ langs.main }}</a></li>
                <li class="active">{{ langs.contact }}</li>
            </ol>
        </div>
    </section>

    <!-- Content section -->
    <section class="content">
        <div class="container">
            <form id="contactform" class="contact-form" name="contactform" method="post" novalidate>
                <div class="row">
                    <div class="col-sm-7">
                        <h2 class="text-uppercase text-left">{{ langs.contact_us }}</h2>
                        <div id="success">
                            <p>{{ langs.message_send_success }}</p>
                        </div>
                        <div id="error">
                            <p>{{ langs.message_send_error }}</p>
                        </div>
                        <div class="input-group input-group--wd">
                            <input type="text" class="input--full" name="name">
                            <span class="input-group__bar"></span>
                            <label>{{ langs.name }}</label>
                        </div>
                        <div class="input-group input-group--wd">
                            <input type="text" class="input--full" name="email">
                            <span class="input-group__bar"></span>
                            <label>{{ langs.email }}</label>
                        </div>
                        <div class="input-group input-group--wd">
                            <input type="text" class="input--full" name="subject">
                            <span class="input-group__bar"></span>
                            <label>{{ langs.subject }}</label>
                        </div>
                        <div class="input-group input-group--wd">
                            <textarea class="textarea--full" name="message"></textarea>
                            <span class="input-group__bar"></span>
                            <label>{{ langs.message }}</label>
                        </div>
                        <input type="hidden" name="ajax" value="true">
                        <button type="submit" id="submit" class="btn btn--wd text-uppercase wave">{{ langs.send_message }}</button>
                    </div>
                    <div class="col-sm-5 text-left">
                        <h2 class="text-uppercase">{{ langs.contact_info }}</h2>
                        <p>{{ contact.contact_desc }}</p>
                        <p class="basic-contact">
                            <i class="fa fa-map-marker" aria-hidden="true"></i> {{ langs.address }}: {{ contact.contact_address }}<br/>
                            <i class="fa fa-phone" aria-hidden="true"></i>{{ langs.phone }}: {{ contact.contact_phone1 }}{{ contact.contact_phone2 ? '; ' ~ contact.contact_phone2 }}<br/>
                            <i class="fa fa-envelope" aria-hidden="true"></i> {{ langs.email }}: <a href="#">{{ contact.contact_email1 }}</a></br>
                            <i class="fa fa-skype" aria-hidden="true"></i> Skype: {{ contact.contact_skype }}</br>
                        </p>
                    </div>
                </div>
            </form>
        </div>
    </section>
    <section class="content fullwidth top-null bottom-null">
        <div id="map"></div>
    </section>
    <script>
        {% set loc = contact.contact_location | split(',') %}
        window.map = false;
        function initialize() {
            var myLatlng = new google.maps.LatLng({{ loc[0] }} , {{ loc[1] }});
            var mapOptions = {
                zoom:{{ loc[2] }},
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                scrollwheel: false,
                streetViewControl: false,
                overviewMapControl: false,
                mapTypeControl: false
            };

            window.map = new google.maps.Map(document.getElementById('map'), mapOptions);

            var contentString = '<div id="content" style="height:40px;">' +
                    '<div>' +
                    '</div>' +
                    '<h6 class="firstHeading" style="font-size:18px;margin:0;">{{ contact.contact_name }}</h6>' +
                    '<div id="bodyContent">' +
                    '</div>' +
                    '</div>';

            var infowindow = new google.maps.InfoWindow({
                content: contentString
            });

            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                title: '{{ contact.contact_name }}'
            });
            //infowindow.open(map, marker);

            google.maps.event.addListener(marker, 'click', function () {
                infowindow.open(map, marker);
            });
        }
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
    <!-- End Content section -->
{% endblock %}

{% block footer %}
    <!-- jQuery form validation -->
    <script src="{{ constants.THEME }}assets/js/build/plugins/form/jquery.form.min.js{{ constants.ver }}"></script>
    <script src="{{ constants.THEME }}assets/js/build/plugins/form/jquery.validate.min.js{{ constants.ver }}"></script>
    <script src="{{ constants.THEME }}assets/js/build/view/contact/contact.min.js{{ constants.ver }}"></script>
{% endblock %}
