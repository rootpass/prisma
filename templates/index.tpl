{% spaceless %}
    <!DOCTYPE html>
    <html lang="{{ globals.lang.code }}">
    <head>
        <title>{{ meta.title }}</title>
        <!-- Favicon -->
        <link rel="icon" type="image/png" href="{{ constants.THEME }}assets/images/favicon.png{{ constants.ver }}" async/>
        <!-- Meta tags -->
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="language" content="{{ globals.lang.code }}"/>
        <meta name="description" content="{{ meta.desc }}"/>
        <meta name="keywords" content="{{ meta.key }}"/>
        <meta name="owner" content="fufala.ge"/>
        <meta name="copyright" content="Copyright &copy; {{ date('Y') }} fufala.ge"/>
        <!-- Facebook Meta tags -->
        {% block facebook %}
        {% endblock %}
        <!-- Mobile Specific Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="shortcut icon" href='assets/images/favicon.png' />
        <link rel='stylesheet' type='text/css' href='{{ constants.THEME }}assets/css/build/bootstrap.min.css{{ constants.ver }}'>
        <link rel='stylesheet' type='text/css' href='{{ constants.THEME }}assets/css/build/font-awesome.min.css{{ constants.ver }}'/>
        <link rel='stylesheet' type='text/css' href='{{ constants.THEME }}assets/css/build/themify-icons.css{{ constants.ver }}'/>
        <link rel='stylesheet' type='text/css' href='{{ constants.THEME }}assets/css/build/flaticon.css{{ constants.ver }}'/>
        <link rel='stylesheet' type='text/css' href='{{ constants.THEME }}assets/css/build/owl.carousel.css{{ constants.ver }}'/>
        <link rel='stylesheet' type='text/css' href='{{ constants.THEME }}assets/css/build/slick.css{{ constants.ver }}'/>
        <link rel='stylesheet' type='text/css' href='{{ constants.THEME }}assets/css/build/jquery.mmenu.all.css{{ constants.ver }}'/>
        <link rel='stylesheet' type='text/css' href='{{ constants.THEME }}assets/css/build/lightbox.min.css{{ constants.ver }}'/>
        <link rel='stylesheet' type='text/css' href='{{ constants.THEME }}assets/css/build/chosen.min.css{{ constants.ver }}'/>
        <link rel='stylesheet' type='text/css' href='{{ constants.THEME }}assets/css/build/animate.css{{ constants.ver }}'/>
        <link rel='stylesheet' type='text/css' href='{{ constants.THEME }}assets/css/build/jquery.scrollbar.css{{ constants.ver }}'/>
        <link rel='stylesheet' type='text/css' href='{{ constants.THEME }}assets/css/build/jquery.bxslider.css{{ constants.ver }}'/>
        <link rel='stylesheet' type='text/css' href='{{ constants.THEME }}assets/css/build/style.css{{ constants.ver }}'/>

        <link href="https://fonts.googleapis.com/css?family=Arimo:400,400i,700|Great+Vibes|Montserrat:400,700|Open+Sans:400,400i,600,600i,700,800i" rel="stylesheet">
        <!--[if IE]>
        <link rel="stylesheet" href="css/ie.css">
        <![endif]-->
        <!--[if lte IE 8]>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.3.0/respond.js{{ constants.ver }}"></script>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.js{{ constants.ver }}"></script>
        <![endif]-->
        <!-- Modernizr -->
        <script src="{{ constants.THEME }}assets/js/build/plugins/modernizr/modernizr.min.js{{ constants.ver }}"></script>
        <style>
            {% block css %}

            {% endblock %}
        </style>
        {% block head %}

        {% endblock %}
        <script type="text/javascript">
            var LANG_ID = {{ globals.lang.id }};
            var LANG = '{{ globals.lang.code }}';
            var THEME = '{{ constants.THEME }}';
            var URL = '{{ globals.url }}';
            var URI = '{{ globals.uri }}';
        </script>
    </head>
    <body>
<!--     {% if siteMessage.status_id %}
        <div class="site-message alert-info">{{ siteMessage.text }}</div>
        {% set ips = siteMessage.ip_address | split(',') %}
        {% if siteMessage.offline and globals.ip not in ips %}
            {{ exit() }}
        {% endif %}
    {% endif %} -->
    <!-- Modal Search -->

    <!-- / end Modal Search -->
    <div class="wrapper">
        <!-- Header section -->
        <header class="header header-basic header-style_12 menu-no-transparent header-sticky">
        <div class="header-top">
            <div class="container">
                <div class="header-top-content">
                    <!-- Header call -->
                    <div class="header-top-left"> <span class="number-phone">+(00) 123 456 789</span></div>
                    <!-- End Header call -->
                    <!-- Header Right -->
                    <div class="header-top-right">
                        <!-- Navigation Top Bar  -->
                        <nav class="top-bar-navigation" id="top-bar-navigation">
                            <ul class="top_bar-nav">
                                <li class="menu-item"><a href="#">პერსონალური ინფორმაცია</a></li>
                                <li class="menu-item"><a href="checkout.html">ყიდვა</a></li>
                                <li class="menu-item menu-item-has-children"><a href="login">შესვლა / რეგისტრაცია</a>
                                    <ul class="sub-menu">
                                        <li class="menu-item"><a href="login.html">რეგისტრაცია</a></li>
                                        <li class="menu-item"><a href="login.html">შესვლა</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                        <!--End Navigation Top Bar  -->
                        <!-- Language Switcher -->
                        <div class="language-switcher-wrap">
                            <ul class="language-flag-switcher">
                                <li class="current-lang lang-switcher-li">
                                    <img width="18" height="12" alt="en" src="{{ constants.THEME }}assets/images/eng.png">ქართული <span class="icon-lang"><i class="fa fa-angle-down"></i></span>
                                    <ul class="language-switcher-inner">
                                        <li class="lang-switcher-li">
                                            <a href="#">
                                                <img width="18" height="12" alt="fr" src="{{ constants.THEME }}assets/images/fr.png"> ინგლისური
                                            </a>
                                        </li>
                                        <li class="lang-switcher-li">
                                            <a href="#">
                                                <img width="18" height="12" alt="usa" src="{{ constants.THEME }}assets/images/usa1.png"> რუსული
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <!-- End Language Switcher -->
                    </div>
                    <!-- End Header call -->
                </div>
            </div>
        </div>
        <div class="main-header">
            <div class="container">
                <div class="row">
                    <div class="col-sm-5 header-nav-left text-right">
                        <!-- Left Nav Menu -->
                        <nav id="left-navigation" class="left-navigation head-nav-extra">
                            <div id="main-menu-left" class="main-nav main-menu">
                                <ul class="menu-nav">
                                    <li class="menu-item"><a href="index.html">მთავარი</a></li>
                                    <li class="menu-item"><a href="search.html">პროდუქტები</a></li></li>
                                </ul>
                            </div>
                        </nav>
                        <!-- End Left Nav Menu -->
                    </div>
                    <div class="col-sm-2 header-logo text-center">
                        <div class="logo">
                            <a href="index.html">
                                <img src="{{ constants.THEME }}assets/images/logo.png" alt="logo">
                            </a>
                        </div><!-- End Logo -->
                    </div>
                    <div class="col-sm-5 header-nav-right text-left" >
                        <!-- Right Nav Menu -->
                        <nav id="right-navigation-right" class="right-navigation head-nav-extra">
                            <div id="main-menu" class="main-nav main-menu">
                                <ul class="menu-nav">
                                    <li class="menu-item"><a href="blog.html">შენი სურვილისამებრ</a></li>
                                    <li class="menu-item"><a href="contact.html">კონტაქტი</a></li>
                                </ul>
                            </div>
                        </nav>
                        <!-- End Right Nav Menu -->
                        <!-- Header Right -->
                        <div class="header-right">
                            <!-- Search Element -->
                            <div class="header-search header-element">
                                <span class="icon-search header-icon" data-togole="header-search"><i class="flaticon-search"></i></span>
                                <div id="header-search" class="header-element-content">
                                    <h4 class="title-element">ძიება!</h4>
                                    <!-- Search Form -->
                                    <form role="search" method="get" action="/search">
                                        <input type="text" value="" placeholder="ძიება..." class="search" id="s" name="q">
                                        <input type="submit" class="button-light" value="ძიება">
                                    </form>
                                    <!-- End Search Form -->
                                </div>
                            </div>
                            <!-- End Search Element -->
                            <!-- Cart Element -->
                            <div class="header-cart header-element">
                                <a href="#"><span class="icon-cart header-icon" data-togole="header-cart"><i class="flaticon-bag"></i></span></a>
                                <span class="cart-number-items"> 5 </span>
                                <div id="header-cart" class="header-element-content">
                                    <h4 class="title-element">Shopping Cart</h4>
                                    <p class="description-cart">You have <span>3 item(s)</span> in your cart</p>
                                    <div class="xshop-cart_list">
                                        <div class="xshop-cart_list-inner content-scrollbar">
                                            <ul class="cart_list product_list_widget">
                                                <li class="mini_cart_item">
                                                    <a class="cart-remove" href="#"></a>
                                                    <a href="#" class="cart-img">
                                                        <img width="100" height="120" src="{{ constants.THEME }}assets/images/img-cart1.jpg" alt="img">
                                                    </a>
                                                    <div class="cart-inner">
                                                        <a href="#">Crew Neck T-Shirt SS in Ash</a>
                                                        <div class="quantity">
                                                            <p class="product-price">$ 120.00
                                                                <span class="quantity-input"> (x <input type="text" size="1" class="input-text qty text" title="Qty" value="200" disabled="disabled"> ) </span>
                                                            </p>
                                                            <div class="xshop-quantity">
                                                                <div class=" buttons_added">
                                                                    <a class="sign minus" href="#"></a>
                                                                    <a class="sign plus" href="#"></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="mini_cart_item">
                                                    <a class="cart-remove" href="#"></a>
                                                    <a href="#" class="cart-img">
                                                        <img width="100" height="120" src="{{ constants.THEME }}assets/images/img-cart2.jpg" alt="img">
                                                    </a>
                                                    <div class="cart-inner">
                                                        <a href="#">Sweatshirt In Grey Marl</a>
                                                        <div class="quantity">
                                                            <p class="product-price">
                                                                <ins><span class="price-amount amount">$80.00 </span></ins>
                                                                <del><span class="price-amount amount">$120.00</span></del>
                                                            </p>
                                                            <div class="xshop-quantity">
                                                                <div class=" buttons_added">
                                                                    <a class="sign minus" href="#"></a>
                                                                    <a class="sign plus" href="#"></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="mini_cart_item">
                                                    <a class="cart-remove" href="#"></a>
                                                    <a href="#" class="cart-img">
                                                        <img width="100" height="120" src="{{ constants.THEME }}assets/images/img-cart1.jpg" alt="img">
                                                    </a>
                                                    <div class="cart-inner">
                                                        <a href="#">Crew Neck T-Shirt SS in Ash</a>
                                                        <div class="quantity">
                                                            <p class="product-price">
                                                                <ins><span class="price-amount amount">$80.00 </span></ins>
                                                                <del><span class="price-amount amount">$120.00</span></del>
                                                            </p>
                                                            <div class="xshop-quantity">
                                                                <div class=" buttons_added">
                                                                    <a class="sign minus" href="#"></a>
                                                                    <a class="sign plus" href="#"></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul><!-- end product list -->
                                        </div>
                                        <div class="mini-cart-bottom">
                                            <p class="total">Subtotal <span class="subtotal-price price-amount amount">$200.00</span></p>
                                            <p class="buttons">
                                                <a class="button checkout" href="checkoput.html">Proceed to checkout</a>
                                                <a class="button " href="cart.html">Go to cart</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Cart Element -->
                            <!-- Button Menu Mobile -->
                            <a href="#" class="mobile-navigation">
                                <span class="button-icon">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </span>
                            </a>
                            <!-- End Button Menu Mobile -->
                        </div>
                        <!-- End Header Right -->
                    </div>
                    <!-- Menu On Mobile -->
                    <div class="menu-mobile-extra"></div>
                    <!-- End Menu On Mobile -->
                </div>
            </div>
        </div>
        <!-- Header Banner -->
        <div class="header-banner banner-slide">
            <div class="xshop-owl-carousel banner-wrap banner-radius rev_slider_wrapper nav-control-style2" data-navControl="yes" data-number="1" data-autoPlay="yes">
                <div class="item-slide">
                    <figure>
                        <img src="{{ constants.THEME }}assets/images/slide-home13/newcover.jpg" alt="img">
                    </figure>
                    <div class="slide-content content-slide-v11 text-center">
                        <span class="link-slide">Simple is the Best</span>
                        <h2 class="title-banner-smaller">Creative Home decor<br>
                            Furniture idea</h2>
                        <a href="#" class="button-slide-radius">Enjoy Now</a>
                    </div>
                </div>
                <div class="item-slide">
                    <figure>
                        <img src="{{ constants.THEME }}assets/images/slide-home13/newcover1.png" alt="img">
                    </figure>
                    <div class="slide-content content-slide-v11 text-center">
                        <span class="link-slide">Simple is the Best</span>
                        <h2 class="title-banner-smaller">Creative Home decor<br>
                            Furniture idea</h2>
                        <a href="#" class="button-slide-radius">Enjoy Now</a>
                    </div>
                </div>
                <div class="item-slide">
                    <figure>
                        <img src="{{ constants.THEME }}assets/images/slide-home13/slide28.jpg" alt="img">
                    </figure>
                    <div class="slide-content content-slide-v11 text-center">
                        <span class="link-slide">Simple is the Best</span>
                        <h2 class="title-banner-smaller">Creative Home decor<br>
                            Furniture idea</h2>
                        <a href="#" class="button-slide-radius">Enjoy Now</a>
                    </div>
                </div>
                <div class="item-slide">
                    <figure>
                        <img src="{{ constants.THEME }}assets/images/slide-home13/slide29.jpg" alt="img">
                    </figure>
                    <div class="slide-content content-slide-v11 text-center">
                        <span class="link-slide">Create Your Own!</span>
                        <h2 class="title-banner-smaller">A Complete collection<br>
                            of X.shop</h2>
                        <a href="#" class="button-slide-radius">Enjoy Now</a>
                    </div>
                </div>
                <div class="item-slide">
                    <figure>
                        <img src="{{ constants.THEME }}assets/images/slide-home13/slide30.jpg" alt="img">
                    </figure>
                    <div class="slide-content content-slide-v11 text-center">
                        <span class="link-slide">Inspire Creativity!</span>
                        <h2 class="title-banner-smaller">Creative Home decor &<br>
                            accessories</h2>
                        <a href="#" class="button-slide-radius">Enjoy Now</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Header Banner -->
    </header>
        <!-- End Header section -->
        {% block content %}
        {% endblock %}
    <footer class="footer footer-style_12">
        <!-- Footer style 12 Top -->
        <div class="footer-top">
            <div class="container">
                <div class="footer-top-inner">
                    <div class="row">
                        <div class="col-sm-3">
                            <h6 class="title-footer">დახმარება</h6>
                            <div class="menu-footer footer-left">
                                <ul class="menu">
                                    <li class="menu-item"><a href="#">Returns</a></li>
                                    <li class="menu-item"><a href="#">Delivery</a></li>
                                    <li class="menu-item"><a href="#">My Account</a></li>
                                    <li class="menu-item"><a href="#">Contact Us</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <h6 class="title-footer">ინფორმაცია</h6>
                            <div class="menu-footer footer-left">
                                <ul class="menu">
                                    <li class="menu-item"><a href="#">Careers</a></li>
                                    <li class="menu-item"><a href="#">Investor Relation</a></li>
                                    <li class="menu-item"><a href="#">Press Releases</a></li>
                                    <li class="menu-item"><a href="#">Shop witn Points</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <h6 class="title-footer">მომხმარებლებისათვის</h6>
                            <div class="menu-footer footer-left">
                                <ul class="menu">
                                    <li class="menu-item"><a href="#">Returns</a></li>
                                    <li class="menu-item"><a href="#">Shipping Info</a></li>
                                    <li class="menu-item"><a href="#">Gift Cards</a></li>
                                    <li class="menu-item"><a href="#">Size Guid</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <h6 class="title-footer">გამოიწერე</h6>
                            <!-- newsletter-form -->
                            <div class="newsletter-style5">
                                <div class="newsletter-form">
                                    <form method="post" action="#">
                                        <div class="frm-wrap newsletter-content">
                                            <input type="email" name="email" placeholder="Your email here">
                                            <button class="submit-button" type="submit"><i class="fa fa-arrow-right"></i></button>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <!-- End newsletter-form -->
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!-- End Footer style 12 Top -->
        <!-- Footer style 12 Bottom -->
        <div class="footer-bottom">
            <div class="container">
                <div class="footer-bottom-left">
                    <p class="copyright"> © 2016 X.Shop. All Right reserved </p>
                </div>
                <div class="footer-bottom-right">
                    <span class="title-socials">Get in touch!</span>
                    <ul class="socials">
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- End Footer style 12 Bottom -->
        <a class="backtotop" href="#">
            <span class="icon-top fa fa-angle-up"></span> Top
        </a>
    </footer>

    <script>
        {{ load('lang') | raw }}
    </script>

    <script type='text/javascript' src='{{ constants.THEME }}assets/js/build/jquery.min.js{{ constants.ver }}'></script>
    <script type='text/javascript' src='{{ constants.THEME }}assets/js/build/owl.carousel.min.js{{ constants.ver }}'></script>
    <script type='text/javascript' src='{{ constants.THEME }}assets/js/build/bootstrap.min.js{{ constants.ver }}'></script>
    <script type='text/javascript' src='{{ constants.THEME }}assets/js/build/slick.js{{ constants.ver }}'></script>
    <script type='text/javascript' src='{{ constants.THEME }}assets/js/build/wow.min.js{{ constants.ver }}'></script>
    <script type='text/javascript' src='{{ constants.THEME }}assets/js/build/jquery.mmenu.all.min.js{{ constants.ver }}'></script>
    <script type='text/javascript' src='{{ constants.THEME }}assets/js/build/lightbox.min.js{{ constants.ver }}'></script>
    <script type='text/javascript' src='{{ constants.THEME }}assets/js/build/jquery.scrollbar.js{{ constants.ver }}'></script>
    <script type='text/javascript' src='{{ constants.THEME }}assets/js/build/chosen.jquery.min.js{{ constants.ver }}'></script>
    <script type='text/javascript' src='{{ constants.THEME }}assets/js/build/jquery-ui.min.js{{ constants.ver }}'></script>
    <script type='text/javascript' src='{{ constants.THEME }}assets/js/build/jquery.bxslider.min.js{{ constants.ver }}'></script>
    <script type='text/javascript' src='{{ constants.THEME }}assets/js/build/jquery.countdown.min.js{{ constants.ver }}'></script>
    <script type='text/javascript' src='{{ constants.THEME }}assets/js/build/frontend.js{{ constants.ver }}'></script>
    <script type='text/javascript' src='{{ constants.THEME }}assets/js/build/frontend-plugin.js{{ constants.ver }}'></script>
    </body>
    </html>
{% endspaceless %}